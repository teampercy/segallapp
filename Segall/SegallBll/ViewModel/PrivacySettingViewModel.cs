﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SegallBll.ViewModel
{
    public class PrivacySettingViewModel
    {
        #region Properties

        User user { get; set; }

        #endregion

        #region Constructors

        public PrivacySettingViewModel()
        {
            this.user = new User();
        }

        #endregion
    }
}
