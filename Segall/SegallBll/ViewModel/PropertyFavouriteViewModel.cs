﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SegallBll.DAL;

namespace SegallBll.ViewModel
{
    public class PropertyFavouriteViewModel
    {
        public List<PropertyFavoriteDB> PropertyFavourite { get; set; } 
        public List<usp_GetPropertyFavouriteUserList_Result> PropertyFavouriteList { get; set; }
    }
}
