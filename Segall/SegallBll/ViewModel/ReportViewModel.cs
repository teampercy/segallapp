﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SegallBll.DAL;

namespace SegallBll.ViewModel
{
    public class ReportViewModel
    {
        public string htmlString { get; set; }
        public string sales { get; set; }
        public string period { get; set; }
        public string GlA { get; set; }
        public string Rent { get; set; }
        public string Net { get; set; }
        public string TI { get; set; }
        public string FreeRent { get; set; }
        public string Term { get; set; }
        public List<Int32> PropertyToSkip { get; set; }
        public string CategoryName { get; set; }
        public int CurrentPage { get; set; }
        public int VolumeOrLease { get; set; }

        public List<PropertySearchResult> propertySearch { get; set; }
        public PropertyFilterDB propertyFilter { get; set; }


        public ReportViewModel()
        {
            this.propertySearch = new List<PropertySearchResult>();
            this.propertyFilter = new PropertyFilterDB();
            this.PropertyToSkip = new List<int>();
        }
    }
}
