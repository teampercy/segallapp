﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SegallBll.DAL;

namespace SegallBll.ViewModel
{
    public class PropertyDetailViewModel
    {
        private SegallEntities sgl = new SegallEntities();
        public PropertyBasic propertybasic {get;set;}
        public PropertySalesVolume propertySalesVolume { get; set; }
        public SalesPeriod salesPeriod { get; set; }
        public usp_PropertyInfoGet_Result propertyInfo { get; set; }
        public PropertyDeal propertydeal { get; set; }
        public Int64 LoggedInUser { get; set; }
        public string Location { get; set; }
        public PropertyDetailDB propertydetail { get; set; }
        public Int64 PropertyId { get; set; }
        public Int64 SalesVolumeId { get; set; }
        public Int64? PropertyDealId { get; set; }
        public object Users { get; set; }

        //Lists
       
        public List<SelectListItem> SizeList = new List<SelectListItem>();
        public List<SelectListItem> CategoryList = new List<SelectListItem>();
        public List<SelectListItem> SalesPeriodList = new List<SelectListItem>();
        public List<SelectListItem> TIChargeList = new List<SelectListItem>();
        public List<SelectListItem> FreeRentList = new List<SelectListItem>();
        public List<SelectListItem> RentList = new List<SelectListItem>();
        public List<SelectListItem> SalesTypeList = new List<SelectListItem>();
        public List<SelectListItem> DealTypeList = new List<SelectListItem>();
        public List<SelectListItem> SalesTransactionList = new List<SelectListItem>();
        public List<SelectListItem> LeaseTransactionList = new List<SelectListItem>();
        public List<SelectListItem> StateList = new List<SelectListItem>();
        public List<PropertyDeal> lstPropertyDeal { get; set; }
        public List<SelectListItem> SourceTypeList = new List<SelectListItem>();
        public List<SelectListItem> LeaseSourceTypeList = new List<SelectListItem>();
        public List<SelectListItem> UsersActiveList = new List<SelectListItem>();
        public List<SelectListItem> RentalRateList = new List<SelectListItem>();

        //Constructors
        public PropertyDetailViewModel()
        {
            this.propertybasic = new PropertyBasic();
            this.propertySalesVolume = new PropertySalesVolume();
            this.propertydeal = new PropertyDeal();
            this.salesPeriod = new SalesPeriod();
            this.propertyInfo = new usp_PropertyInfoGet_Result();
            this.lstPropertyDeal = new List<PropertyDeal>();
            this.PropertyId = 0;
            populateLists();

        }

        public PropertyDetailViewModel(PropertyDetailViewModel viewmodel)
        {
            propertydetail = viewmodel.propertydetail;
            this.propertybasic = new PropertyBasic();
            this.propertySalesVolume = new PropertySalesVolume();
            this.propertydeal = new PropertyDeal();
            this.salesPeriod = new SalesPeriod();
            this.propertyInfo = new usp_PropertyInfoGet_Result();
            this.lstPropertyDeal = new List<PropertyDeal>();
            populateLists();

        }


        //Methods
        public void populateLists()
        {
            //Added by Jaywanti Jain on 20-07-2016
            var RentalRateListData = from r in sgl.usp_GetTypeValues()
                                 where r.TypeValueName.Equals("RentalRate")
                                 select r;
            if (RentalRateListData != null)
            {
                foreach (var r in RentalRateListData)
                {
                    RentalRateList.Add(new SelectListItem { Text = r.TypeValueDesc, Value = r.TypeValue.ToString() });
                }
            }
            //
            foreach (usp_SizeGetList_Result size in sgl.usp_SizeGetList())
            {
                SizeList.Add(new SelectListItem { Text = size.TypeValueDesc, Value = size.TypeValue.ToString() });
            }

            CategoryList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (usp_CategoryGetList_Result category in sgl.usp_CategoryGetList())
            {
                CategoryList.Add(new SelectListItem { Text = category.TypeValueDesc, Value = category.TypeValue.ToString() });
            }

            SalesPeriodList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (usp_SalesPeriodsGetList_Result salesperiod in sgl.usp_SalesPeriodsGetList())
            {
                SalesPeriodList.Add(new SelectListItem { Text = salesperiod.Period, Value = salesperiod.SalesPeriodId.ToString() });
            }

            foreach (usp_RentGetList_Result rent in sgl.usp_RentGetList())
            {
                RentList.Add(new SelectListItem { Text = rent.TypeValueDesc.ToString(), Value = rent.TypeValue.ToString() });
            }

            foreach (usp_TIChargeGetList_Result ticharge in sgl.usp_TIChargeGetList())
            {
                TIChargeList.Add(new SelectListItem { Text = ticharge.TypeValueDesc.ToString(), Value = ticharge.TypeValue.ToString() });
            }

            foreach (usp_FreeRentGetList_Result freerent in sgl.usp_FreeRentGetList())
            {
                FreeRentList.Add(new SelectListItem { Text = freerent.TypeValueDesc.ToString(), Value = freerent.TypeValue.ToString() });
            }

            SalesTypeList.Add(new SelectListItem {Text = "Weekly", Value = "1" });
            SalesTypeList.Add(new SelectListItem { Text = "Monthly", Value = "2" });
            SalesTypeList.Add(new SelectListItem { Text = "Anually", Value = "3" });

            DealTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            DealTypeList.Add(new SelectListItem { Text = "Sales", Value = "1" });
            DealTypeList.Add(new SelectListItem { Text = "Lease", Value = "2" });

            SalesTransactionList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            SalesTransactionList.Add(new SelectListItem { Text = "Improved", Value = "1" });
            SalesTransactionList.Add(new SelectListItem { Text = "UnImproved", Value = "2" });

            LeaseTransactionList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            LeaseTransactionList.Add(new SelectListItem { Text = "Building", Value = "3" });
            LeaseTransactionList.Add(new SelectListItem { Text = "Ground", Value = "4" });

            StateList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (usp_tbl_StateList_Result state in sgl.usp_tbl_StateList())
            {
                StateList.Add(new SelectListItem { Text = state.StateName, Value = state.State.ToString() });
            }

            SourceTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            SourceTypeList.Add(new SelectListItem { Text = "Tenant Sales Report", Value = "Tenant Sales Report" });
            SourceTypeList.Add(new SelectListItem { Text = "Broker", Value = "Broker" });
            SourceTypeList.Add(new SelectListItem { Text = "Tenant", Value = "Tenant" });
            SourceTypeList.Add(new SelectListItem { Text = "Landlord", Value = "Landlord" });
            SourceTypeList.Add(new SelectListItem { Text = "Consultant", Value = "Consultant" });

            LeaseSourceTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            LeaseSourceTypeList.Add(new SelectListItem { Text = "Tenant/Broker", Value = "Tenant/Broker" });
            LeaseSourceTypeList.Add(new SelectListItem { Text = "Landlord/Broker", Value = "Landlord/Broker" });
            LeaseSourceTypeList.Add(new SelectListItem { Text = "Market Sales", Value = "Market Sales" });

            UsersActiveList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            var userActiveList = from u in sgl.Users
                                 where u.IsActive==true
                                 select u;
            if (userActiveList != null)
            {
                foreach (var u in userActiveList)
                {
                    UsersActiveList.Add(new SelectListItem { Text = u.Firstname + " " + u.Lastname, Value = u.UserId.ToString() });
                }
            }


        }


    }
}
