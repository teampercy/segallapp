﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SegallBll.DAL;

namespace SegallBll.ViewModel
{
    public class PropertyInfoViewModel
    {
        public PropertyInfoDetails propertyInfoDetails { get; set; }
        public List<usp_PropertyAccessRightsDetailsForUser_Result> propertyaccessright { get; set; }
        public bool SalesVolume { get; set; }
        public bool RentInformation { get; set; }
    }
}
