﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SegallBll.ViewModel
{
    public class EmailViewModel
    {
        public string EmailFrom { get; set; }
        public string Emailto { get; set; }
        public string subject { get; set; }
        public string Body { get; set; }
        public string AttachmentPath { get; set; }
        public string ReportName { get; set; }
    }
}
