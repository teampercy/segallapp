﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SegallBll.ViewModel
{
    public class LoginViewModel
    {
        private SegallEntities sgl = new SegallEntities();
        public User user { get; set; }
        public List<usp_GetUserAll_Result> userall { get; set; }

        public LoginViewModel()
        {
            this.user = new User();
        }
    }
}
