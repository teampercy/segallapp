﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SegallBll.Custom_Class
{
    public static class Encryption
    {
        public static string encriptString(string originalString)
        {
            string encryptedString = "";
            originalString = originalString.Trim();
            if (originalString.Length == 0)
            {
                encryptedString = "993062892";
            }
            else
            {
                foreach (char currCharacter in originalString.ToCharArray())
                {
                    encryptedString += ((Int32)currCharacter).ToString().PadLeft(3, '0');
                }
            }
            return encryptedString;
        }

        public static string decryptString(string encryptedString)
        {
            string originalString = "";
            if (encryptedString == "993062892")
            {
                originalString = "";
            }
            else
            {
                for (int iCnt = 0; iCnt < encryptedString.Length; iCnt += 3)
                {
                    originalString += ((char)Convert.ToInt32(encryptedString.Substring(iCnt, 3))).ToString();
                }
            }
            return originalString;
        }
    }
}
