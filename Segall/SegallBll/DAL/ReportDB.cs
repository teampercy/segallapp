﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SegallBll.DAL
{
    public class ReportDB
    {
        #region Objects

        SegallEntities sgl;
        PropertyBasic propertyBasic;
        PropertySalesVolume propertysalesvolume;
        PropertyDeal propertydeal;
        IEnumerable<usp_PropertiesSearch_Result> PropertiesSearch { get; set; }
        

        #endregion

        public List<PropertySearchResult> GenerateReport(string location, Int32 distance, Int64 loggedInUserId)
        {
          
            sgl = new SegallEntities();
            //object[] report = new object[2];
            List<PropertySearchResult> propertySearchResult = new List<PropertySearchResult>();
            try
            {
                var obj = sgl.usp_PropertiesSearch(loggedInUserId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,null,null,null,null,null, location, distance, null, 100,false).ToList();
                foreach (var property in obj)
                {
                    PropertySearchResult propertysearch = new PropertySearchResult();
                    propertysearch.propertySearch = property;
                    propertysearch.propertyDeal = sgl.usp_GetPropertyDeals(Convert.ToInt64(property.PropertyId)).ToList();
                    propertysearch.propertySalesVolume = sgl.usp_GetPropertySalesVolume(Convert.ToInt64(property.PropertyId)).ToList();
                    propertySearchResult.Add(propertysearch);
                }
                //report[1] = sgl.usp_GetTypeValues().ToList();
                ////report[2] = sgl.usp_GetPropertyDeals(

            }
            catch (Exception ex)
            { }
            return propertySearchResult;
        }
    }
}
