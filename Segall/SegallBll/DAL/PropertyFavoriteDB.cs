﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Objects;

namespace SegallBll.DAL
{
    public class PropertyFavoriteDB
    {
        SegallEntities sgl;
        public List<usp_GetPropertyFavouriteUserList_Result> GetPropertyFavouriteUserList;
        public void InsertFavourite(Int64 propertyid, Int64 UserId)
        {
            sgl = new SegallEntities();
            ObjectParameter PropertyFavouriteId = new ObjectParameter("PropertyFavouriteId",DbType.Int64);
            try
            {
                sgl.usp_PropertyFavouritesInsert(PropertyFavouriteId, propertyid, UserId);
            }
            catch (Exception ex)
            { }
        }
        public void DeleteFavourite(Int64 propertyid, Int64 UserId)
        {
            sgl = new SegallEntities();
            try
            {
                sgl.usp_PropertyFavouritesDelete(propertyid, UserId);
            }
            catch (Exception ex)
            { }
        }

        public void DeleteFavouriteALL(Int64 UserId)
        {
            sgl = new SegallEntities();
            try
            {
                sgl.usp_DeleteAllFavourites(UserId);
            }
            catch (Exception ex)
            { }
        }
        public List<usp_GetPropertyFavouriteUserList_Result> GetFavouriteByUserid(Int64 UserId)
        {
            sgl = new SegallEntities();
            GetPropertyFavouriteUserList = sgl.usp_GetPropertyFavouriteUserList(UserId).ToList();
            return GetPropertyFavouriteUserList;
        }
        
    }
}
