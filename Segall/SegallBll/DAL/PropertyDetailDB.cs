﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace SegallBll.DAL
{
    public class PropertyDetailDB
    {
        public List<usp_PropertyInfoGet_Result> propertybasicresult { get; set; }
        public IEnumerable<usp_GetPropertyVolumes_Result> propertysalesvolumeresult { get; set; }
        public IEnumerable<usp_GetPropertyDeals_Result> propertydealsresult { get; set; }


        //public static List<PropertyBasic> GetAllProperties()
        //{
        //    var entities = new SegallEntities();
        //    var x = from c in entities.PropertyBasics
        //            select c;
        //    return x.ToList();

        //}

        public static IEnumerable<PropertyBasic> GetAllProperties()
        {
            var db = new SegallEntities();
            return db.PropertyBasics.AsEnumerable();
        }
    }


}
