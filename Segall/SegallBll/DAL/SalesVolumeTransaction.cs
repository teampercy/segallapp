﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SegallBll;

namespace SegallBll.DAL
{
    public class SalesVolumeTransaction
    {
        #region Properties

        public long SalesVolumeId { get; set; }
        public Nullable<long> PropertyId { get; set; }
        public Nullable<decimal> SalesWeekly { get; set; }
        public Nullable<decimal> SalesMonthly { get; set; }
        public Nullable<decimal> SalesAnnually { get; set; }
        public Nullable<decimal> VolumePerSF { get; set; }
        public Nullable<System.DateTime> PeriodFrom { get; set; }
        public Nullable<System.DateTime> PeriodTo { get; set; }
        public Nullable<byte> TransactionType { get; set; }
        public Nullable<int> PeriodYear { get; set; }
        public string SourceType { get; set; }
        public string BrokerName { get; set; }
        public Nullable<long> BrokerId { get; set; }
        #endregion
    }
}
