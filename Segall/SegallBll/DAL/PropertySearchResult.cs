﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SegallBll.DAL
{
    public class PropertySearchResult
    {
        public usp_PropertiesSearch_Result propertySearch { get; set; }
        public IEnumerable<usp_GetPropertyDeals_Result> propertyDeal { get; set; }
        public IEnumerable<usp_GetPropertySalesVolume_Result> propertySalesVolume { get; set; }
    }
}
