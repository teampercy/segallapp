﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Linq;
using System.Text;

namespace SegallBll.DAL
{
    public class PropertySavedSearchDB
    {
        SegallEntities sgl;

        public string savedSearchId { get; set; }
        public string type { get; set; }

        public SavedSearch savesearch { get; set; }
        public PropertySavedSearchDB()
        {
            this.savesearch = new  SavedSearch();
        }

        public object GetSavedSearchAll(Int32 userId)
        {
            sgl = new SegallEntities();
            object savedsearch = new object();
            try
            {
                savedsearch = sgl.usp_GetSavedSearch(userId).ToList();
            }
            catch (Exception ex)
            { }
            return savedsearch;
        }
        public void DeletesavedSearch(Int64 savedSearchId)
        {
            sgl = new SegallEntities();
            try
            {
                sgl.usp_DeleteSavedSearch(savedSearchId);
            }
            catch (Exception ex)
            { }
        }

        public void savedSearch(PropertySavedSearchDB savedSearch)
        {
            sgl = new SegallEntities();
            ObjectParameter objSavedSearchId = new ObjectParameter("SavedSearchId", DbType.Int64);
            try
            {
                sgl.usp_SavedSearchesInsert(objSavedSearchId, savedSearch.savesearch.SavedSearchName, savedSearch.savesearch.SearchSavedBy, savedSearch.savesearch.SearchSavedDate, savedSearch.savesearch.SearchModifiedBy, savedSearch.savesearch.SearchModifiedDate, savedSearch.savesearch.PropertyName, savedSearch.savesearch.City, savedSearch.savesearch.County, savedSearch.savesearch.State, savedSearch.savesearch.Zipcode, savedSearch.savesearch.SizeLower, savedSearch.savesearch.SizeUpper, savedSearch.savesearch.category, savedSearch.savesearch.SalesLower, savedSearch.savesearch.SalesUpper, savedSearch.savesearch.DealType, savedSearch.savesearch.DealSubType, savedSearch.savesearch.LandSizeLower, savedSearch.savesearch.LandSizeUpper, savedSearch.savesearch.BuildingSizeLower, savedSearch.savesearch.BuildingSizeUpper, savedSearch.savesearch.ConsiderationAmtLower, savedSearch.savesearch.ConsiderationAmtUpper, savedSearch.savesearch.RentedSizeLower, savedSearch.savesearch.RentedSizeUpper, savedSearch.savesearch.RentAmountLower, savedSearch.savesearch.RentAmountUpper, savedSearch.savesearch.Location, savedSearch.savesearch.Radius, savedSearch.savesearch.SalesPeriodFrom, savedSearch.savesearch.SalesPeriodTo);
            }
            catch (Exception ex)
            { }
        }

        public usp_GetSavedSearchDetails_Result GetSavedSearchDetail(Int32 savedSearchId)
        {
            sgl = new SegallEntities();
            //object savedsearch = new object();
           IEnumerable<usp_GetSavedSearchDetails_Result> savedsearch= null;
            try
            {
                //savedsearch = sgl.usp_GetSavedSearch().ToList();
                savedsearch = sgl.usp_GetSavedSearchDetails(savedSearchId).ToList(); 
            }
            catch (Exception ex)
            { }
            return savedsearch.First() ;
        }
    }
}
