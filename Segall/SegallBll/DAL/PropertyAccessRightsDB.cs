﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SegallBll.DAL
{
    public class PropertyAccessRightsDB
    {
        #region Objects

        SegallEntities sgl;

        #endregion

        #region Properties

        public string SalesUserids { get; set; }
        public string RentUserids { get; set; }
        public Int64 PropertyId { get; set; }

        #endregion

        #region Methods

        public object GetUsers(Int64 PropertyId)
        {
            sgl = new SegallEntities();
            object users = new object();

            try
            {
                users = sgl.usp_GetUsers(PropertyId).ToList();

            }
            catch (Exception ex)
            { }
            return users;
        }

        public void UpdatePropertyAccessRights(Int64 PropertyId, string Userid, string UpdateValueFor)
        {
            sgl = new SegallEntities();
            try
            {
                sgl.usp_PropertyAccessRightsUpdate(PropertyId, Userid, UpdateValueFor);
            }
            catch (Exception ex)
            { }
        }

        #endregion
    }
}
