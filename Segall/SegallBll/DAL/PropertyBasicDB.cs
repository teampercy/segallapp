﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Objects;
using System.Data;

namespace SegallBll.DAL
{
    public class PropertyBasicDB
    {
        #region Objects

        SegallEntities sgl;
        PropertyBasic propertyBasic;
        PropertySalesVolume propertysalesvolume;
        PropertyDeal propertydeal;
       
        

        #endregion

        #region Methods

        public Int64 PropertyBasicAdd(PropertyBasic propertybasic)
        {
            sgl = new SegallEntities();
            propertysalesvolume = new PropertySalesVolume();
            propertydeal = new PropertyDeal();
            foreach (var psv in propertybasic.PropertySalesVolumes)
            {
                propertysalesvolume.SalesWeekly = psv.SalesWeekly;
                propertysalesvolume.SalesMonthly = psv.SalesMonthly;
                propertysalesvolume.SalesAnnually = psv.SalesAnnually;
                propertysalesvolume.VolumePerSF = psv.VolumePerSF;
                propertysalesvolume.PeriodFrom = psv.PeriodFrom;
                propertysalesvolume.PeriodTo = psv.PeriodTo;
                propertysalesvolume.PeriodYear = psv.PeriodYear;
                propertysalesvolume.SourceType = psv.SourceType;
                propertysalesvolume.BrokerName = psv.BrokerName;
                propertysalesvolume.BrokerId = psv.BrokerId;
            }



            ObjectParameter outPropertyBasicId = new ObjectParameter("PropertyId", DbType.Int64);
            ObjectParameter outSalesVolumeId = new ObjectParameter("SalesVolumeId", DbType.Int64);
            ObjectParameter outPropertyDealId = new ObjectParameter("PropertyDealId", DbType.Int64);
            sgl.usp_PropertyBasicInsert(outPropertyBasicId, propertybasic.PropertyName, propertybasic.Size, propertybasic.SizeUnit, propertybasic.Address, propertybasic.City, propertybasic.County, propertybasic.State, propertybasic.Zipcode, propertybasic.Latitude, propertybasic.Longitude, propertybasic.Center, propertybasic.GLA, propertybasic.Land, propertybasic.category, propertybasic.Zoning, propertybasic.PropertyAddedBy,propertybasic.Notes,propertybasic.Tenant);
            Int64 PropertyId = Convert.ToInt64(outPropertyBasicId.Value);
            if (propertysalesvolume.SalesWeekly != null || propertysalesvolume.SalesMonthly != null || propertysalesvolume.SalesAnnually != null || propertysalesvolume.VolumePerSF != null || propertysalesvolume.PeriodFrom != null || propertysalesvolume.PeriodTo != null)
            {
                sgl.usp_PropertySalesVolumeInsert(outSalesVolumeId, PropertyId, propertysalesvolume.SalesWeekly, propertysalesvolume.SalesMonthly, propertysalesvolume.SalesAnnually, propertysalesvolume.VolumePerSF, propertysalesvolume.PeriodFrom, propertysalesvolume.PeriodTo, propertysalesvolume.PeriodYear, propertysalesvolume.SourceType, propertysalesvolume.BrokerName, propertysalesvolume.BrokerId);
            }

            foreach (var pd in propertybasic.PropertyDeals)
            {
                propertydeal.DealType = pd.DealType;
                propertydeal.DealSubType = pd.DealSubType;
                propertydeal.LandSize = pd.LandSize;
                propertydeal.LandSizeUnit = pd.LandSizeUnit;
                propertydeal.BuildingSize = pd.BuildingSize;
                propertydeal.BuildingSizeUnit = pd.BuildingSizeUnit;
                propertydeal.ConsiderationAmt = pd.ConsiderationAmt;
                propertydeal.RentedSize = pd.RentedSize;
                propertydeal.RentAmount = pd.RentAmount;
                propertydeal.RentUnit = pd.RentUnit;
                propertydeal.CAMCharge = pd.CAMCharge;
                propertydeal.Insurance = pd.Insurance;
                propertydeal.Tax = pd.Tax;
                propertydeal.TICharge = pd.TICharge;
                propertydeal.TIUnit = pd.TIUnit;
                propertydeal.FreeRent = pd.FreeRent;
                propertydeal.FreeRentUnit = pd.FreeRentUnit;
                propertydeal.Nets = pd.Nets;
                propertydeal.SourceType = pd.SourceType;
                propertydeal.BrokerName = pd.BrokerName;
                propertydeal.BrokerId = pd.BrokerId;

                sgl.usp_PropertyDealsInsert(outPropertyDealId, PropertyId, propertydeal.DealType, propertydeal.DealSubType, propertydeal.LandSize, propertydeal.LandSizeUnit, propertydeal.BuildingSize, propertydeal.BuildingSizeUnit, propertydeal.ConsiderationAmt, propertydeal.RentedSize, propertydeal.RentAmount, propertydeal.RentUnit, propertydeal.CAMCharge, propertydeal.Insurance, propertydeal.Tax, propertydeal.TICharge, propertydeal.TIUnit, propertydeal.FreeRent, propertydeal.FreeRentUnit,propertydeal.Nets,propertydeal.SourceType,propertydeal.BrokerName,propertydeal.BrokerId);
            }

            return PropertyId;
        }

        public Int64 PropertyBasicUpdate(PropertyBasic propertybasic)
        {
            sgl = new SegallEntities();
            propertysalesvolume = new PropertySalesVolume();
            propertydeal = new PropertyDeal();
           
            //PropertyBasic Update
            ObjectParameter outPropertyIdValue = new ObjectParameter("PropertyIdValue", DbType.Int64);
            sgl.usp_PropertyBasicUpdate(propertybasic.PropertyId, propertybasic.PropertyName, propertybasic.Size, propertybasic.SizeUnit, propertybasic.Address, propertybasic.City, propertybasic.County, propertybasic.State, propertybasic.Zipcode, propertybasic.Latitude, propertybasic.Longitude, propertybasic.Center, propertybasic.GLA, propertybasic.Land, propertybasic.category, propertybasic.Zoning, outPropertyIdValue, propertybasic.PropertyEditedBy, propertybasic.Notes,propertybasic.Tenant);
            Int64 PropertyIdValue = Convert.ToInt64(outPropertyIdValue.Value);

            ////PropertySalesVolume Update
            //ObjectParameter outSalesVolumeIdValue = new ObjectParameter("SalesVolumeIdValue", DbType.Int64);
            //ObjectParameter outSalesVolumeId = new ObjectParameter("SalesVolumeId", DbType.Int64);
            //foreach (var psv in propertybasic.PropertySalesVolumes)
            //{
            //    //if (psv.PeriodFrom != null)
            //    //{

            //        propertysalesvolume.SalesVolumeId = psv.SalesVolumeId;
            //        propertysalesvolume.SalesWeekly = psv.SalesWeekly;
            //        propertysalesvolume.SalesMonthly = psv.SalesMonthly;
            //        propertysalesvolume.SalesAnnually = psv.SalesAnnually;
            //        propertysalesvolume.VolumePerSF = psv.VolumePerSF;
            //        propertysalesvolume.PeriodFrom = psv.PeriodFrom;
            //        propertysalesvolume.PeriodTo = psv.PeriodTo;

            //        if (psv.SalesVolumeId > 0)
            //        {
            //            sgl.usp_PropertySalesVolumeUpdate(propertysalesvolume.SalesVolumeId, propertybasic.PropertyId, propertysalesvolume.SalesWeekly, propertysalesvolume.SalesMonthly, propertysalesvolume.SalesAnnually, propertysalesvolume.VolumePerSF, propertysalesvolume.PeriodFrom, propertysalesvolume.PeriodTo, outSalesVolumeIdValue);
            //        }
            //        else
            //        {
            //            sgl.usp_PropertySalesVolumeInsert(outSalesVolumeId, propertybasic.PropertyId, propertysalesvolume.SalesWeekly, propertysalesvolume.SalesMonthly, propertysalesvolume.SalesAnnually, propertysalesvolume.VolumePerSF, propertysalesvolume.PeriodFrom, propertysalesvolume.PeriodTo);
            //        }

            //    //}
            //}
            //sgl.usp_PropertySalesVolumeUpdate(propertysalesvolume.SalesVolumeId, propertysalesvolume.PropertyId, propertysalesvolume.SalesWeekly, propertysalesvolume.SalesMonthly, propertysalesvolume.SalesAnnually, propertysalesvolume.VolumePerSF, propertysalesvolume.PeriodFrom, propertysalesvolume.PeriodTo,outSalesVolumeIdValue);
            //Int64 SalesVolumeIdValue = Convert.ToInt64(outSalesVolumeIdValue.Value);

            // if (PropertyIdValue > 0 && SalesVolumeIdValue > 0)
            if (PropertyIdValue > 0)
            {
                return PropertyIdValue;
            }
            else
                return 0;
        }

        public PropertyBasic PropertyBasicGet(Int64 propertybasicid)
        {
            propertyBasic = new PropertyBasic();
            return propertyBasic;
        }
        public PropertyInfoDetails PropertyDetailsGet(Int64 PropertyId)
        {
            //object[] Property = new object[4];
            PropertyInfoDetails propertyInfodetail = new PropertyInfoDetails();
            try
            {
                sgl = new SegallEntities();
                
                if (sgl.usp_PropertyInfoGet(PropertyId).Count() > 0)
                {
                    propertyInfodetail.propertyInfo = sgl.usp_PropertyInfoGet(PropertyId).First();
                    propertyInfodetail.propertyDeal = sgl.usp_GetPropertyDeals(PropertyId).ToList();
                    propertyInfodetail.propertySalesVolume = sgl.usp_GetPropertySalesVolume(PropertyId).ToList();
                }
                //if (sgl.usp_GetPropertyVolumes(PropertyId).Count() > 0)
                //{
                //    var propertySalesVolumes = sgl.usp_GetPropertyVolumes(PropertyId).ToList();
                //    Property[1] = propertySalesVolumes;
                //    if (sgl.usp_GetSalesPeriod(propertySalesVolumes[0].SalesPeriodId).Count() > 0)
                //    {
                //        var salesPeriod = sgl.usp_GetSalesPeriod(propertySalesVolumes[0].SalesPeriodId).ToList();
                //        Property[3] = salesPeriod;
                //    }
                //}
                //if (sgl.usp_GetPropertyDeals(PropertyId).Count() > 0)
                //{
                //    var propertyDeals = sgl.usp_GetPropertyDeals(PropertyId).ToList();
                //    Property[2] = propertyDeals;
                //}
            }
            catch (Exception ex)
            { }
            return propertyInfodetail;
        }
        public PropertyDetailDB PropertyDetails(Int64 PropertyId)
        {
            PropertyDetailDB propertydetail = new PropertyDetailDB();
            try
            {
                sgl = new SegallEntities();

                if (sgl.usp_PropertyInfoGet(PropertyId).Count() > 0)
                {
                    propertydetail.propertybasicresult = sgl.usp_PropertyInfoGet(PropertyId).ToList();
                }
                if (sgl.usp_GetPropertyVolumes(PropertyId).Count() > 0)
                {
                    propertydetail.propertysalesvolumeresult = sgl.usp_GetPropertyVolumes(PropertyId).ToList();
                }
                if (sgl.usp_GetPropertyDeals(PropertyId).Count() > 0)
                {
                    propertydetail.propertydealsresult = sgl.usp_GetPropertyDeals(PropertyId).ToList();
                }
            }
            catch (Exception ex)
            { }
            return propertydetail;
        }
     

        public object[] PropertyMapPoint(string location, int radius)
        {
            object[] MapPoint = new object[1];
            try
            {
                sgl = new SegallEntities();
                var mapPoints = sgl.usp_PropertyBasicPlotmapPoint(location, radius).ToList();
                MapPoint[0] = mapPoints;
            }
            catch (Exception ex)
            { }
            return MapPoint;
        }

        public object[] GetPropertybasic()
        {
            object[] propertybasicall = new object[1];
            try
            {
                sgl = new SegallEntities();
            }
            catch (Exception ex)
            { }
            return propertybasicall;
        }

        //public string GetZipCode(string lat, string lng)
        //{
        //    sgl = new SegallEntities();
        //    try
        //    {
                
        //    }
        //    catch (Exception ex)
        //    { 
        //    }
        //    return lat;;
        //}

        public List<string> GetPropertyDetailInfo(string TextToSearch)
        {
            sgl = new SegallEntities();
            List<string> PropertyResults = new List<string>();
            try
            {
               //code to call a SP
            }
            catch (Exception ex)
            { }
            return PropertyResults;
        }
        public string deletePropertyBasic(Int64 PropertyId)
        {
            try
            {
                sgl = new SegallEntities();
                sgl.usp_PropertyBasicDelete(PropertyId);
            }
            catch (Exception ex)
            { }
            return "Property Delete Successfully";
        }
        public int UpdateSizeAndVolume(Int64 PropertyId,decimal Size)
        {
            try
            {
                sgl = new SegallEntities();
                sgl.usp_UpdatePropertyVolume(PropertyId, Size);
                return 1;
            }
            catch (Exception ex)
            { }
            return 0;
        }
        #endregion
    }
}
