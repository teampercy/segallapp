﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using System.Data;

namespace SegallBll.DAL
{
    public class SalesVolumeDB
    {
        #region Objects

        SegallEntities sgl;

        #endregion

        #region Properties

        public Int64 SalesVolumeId { get; set; }

        #endregion

        #region Methods

        public Int64 SalesVolumeInsert(SalesVolumeTransaction salesvolumetransaction)
        {
            sgl = new SegallEntities();
            Int64 salesvolumeid = 0;

            //PropertySalesVolume Insert
            ObjectParameter outSalesVolumeId = new ObjectParameter("SalesVolumeId", DbType.Int64);
            if (salesvolumetransaction.SalesWeekly != null || salesvolumetransaction.SalesMonthly != null || salesvolumetransaction.SalesAnnually != null || salesvolumetransaction.VolumePerSF != null || salesvolumetransaction.PeriodFrom != null || salesvolumetransaction.PeriodTo != null)
            {
                sgl.usp_PropertySalesVolumeInsert(outSalesVolumeId, salesvolumetransaction.PropertyId, salesvolumetransaction.SalesWeekly, salesvolumetransaction.SalesMonthly, salesvolumetransaction.SalesAnnually, salesvolumetransaction.VolumePerSF, salesvolumetransaction.PeriodFrom, salesvolumetransaction.PeriodTo, salesvolumetransaction.PeriodYear, salesvolumetransaction.SourceType, salesvolumetransaction.BrokerName, salesvolumetransaction.BrokerId);
                salesvolumeid = Convert.ToInt64(outSalesVolumeId.Value);
            }


            if (salesvolumeid > 0)
            {
                return salesvolumeid;
            }
            else
                return 0;
        }

        public Int64 SalesVolumeUpdate(SalesVolumeTransaction salesvolumetransaction)
        {
            sgl = new SegallEntities();
            Int64 salesvolumeid = 0;

            //PropertySalesVolume Update
            ObjectParameter outSalesVolumeIdValue = new ObjectParameter("SalesVolumeIdValue", DbType.Int64);
            if (salesvolumetransaction.SalesWeekly != null || salesvolumetransaction.SalesMonthly != null || salesvolumetransaction.SalesAnnually != null || salesvolumetransaction.VolumePerSF != null || salesvolumetransaction.PeriodFrom != null || salesvolumetransaction.PeriodTo != null)
            {
                sgl.usp_PropertySalesVolumeUpdate(salesvolumetransaction.SalesVolumeId, salesvolumetransaction.PropertyId, salesvolumetransaction.SalesWeekly, salesvolumetransaction.SalesMonthly, salesvolumetransaction.SalesAnnually, salesvolumetransaction.VolumePerSF, salesvolumetransaction.PeriodFrom, salesvolumetransaction.PeriodTo, salesvolumetransaction.PeriodYear, salesvolumetransaction.SourceType, salesvolumetransaction.BrokerName, salesvolumetransaction.BrokerId, outSalesVolumeIdValue);
                salesvolumeid = Convert.ToInt64(outSalesVolumeIdValue.Value);
            }

            if (salesvolumeid > 0)
            {
                return salesvolumeid;
            }
            else
                return 0;
        }

        public Int64 SalesVolumeDelete(SalesVolumeTransaction salesvolumetransaction)
        {
            sgl = new SegallEntities();

            //PropertySalesVolume Delete
            ObjectParameter outSalesVolumeIdValue = new ObjectParameter("SalesVolumeIdValue", DbType.Int64);
            sgl.usp_PropertySalesVolumeDelete(salesvolumetransaction.SalesVolumeId, outSalesVolumeIdValue);

            Int64 SalesVolumeIdValue = Convert.ToInt64(outSalesVolumeIdValue.Value);


            if (SalesVolumeIdValue > 0)
            {
                return SalesVolumeIdValue;
            }
            else
                return 0;
        }

        #endregion
    }
}
