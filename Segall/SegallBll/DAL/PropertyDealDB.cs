﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using System.Data;


namespace SegallBll.DAL
{
    public class PropertyDealDB
    {


        #region Objects

        SegallEntities sgl;

        #endregion

        #region Properties

        public Int64 PropertyDealId { get; set; }

        #endregion

        #region Methods

        public Int64 PropertyDealInsert(PropertyDealTransaction propertydeal)
        {
            sgl = new SegallEntities();

            //PropertyDeals Insert
            ObjectParameter outPropertyDealId = new ObjectParameter("PropertyDealId", DbType.Int64);
            sgl.usp_PropertyDealsInsert(outPropertyDealId, propertydeal.PropertyId, propertydeal.DealType, propertydeal.DealSubType, propertydeal.LandSize, propertydeal.LandSizeUnit, propertydeal.BuildingSize, propertydeal.BuildingSizeUnit, propertydeal.ConsiderationAmt, propertydeal.RentedSize, propertydeal.RentAmount, propertydeal.RentUnit, propertydeal.CAMCharge, propertydeal.Insurance, propertydeal.Tax, propertydeal.TICharge, propertydeal.TIUnit, propertydeal.FreeRent, propertydeal.FreeRentUnit,propertydeal.Nets,propertydeal.SourceType,propertydeal.BrokerName,propertydeal.BrokerId);

            Int64 PropertyDealId = Convert.ToInt64(outPropertyDealId.Value);


            if (PropertyDealId > 0)
            {
                return PropertyDealId;
            }
            else
                return 0;
        }

        public Int64 PropertyDealUpdate(PropertyDealTransaction propertydeal)
        {
            sgl = new SegallEntities();

            //PropertyDeals Update
            ObjectParameter outPropertyDealIdValue = new ObjectParameter("PropertyDealIdValue", DbType.Int64);
            sgl.usp_PropertyDealsUpdate(propertydeal.PropertyDealId, propertydeal.PropertyId, propertydeal.DealType, propertydeal.DealSubType, propertydeal.LandSize, propertydeal.LandSizeUnit, propertydeal.BuildingSize, propertydeal.BuildingSizeUnit, propertydeal.ConsiderationAmt, propertydeal.RentedSize, propertydeal.RentAmount, propertydeal.RentUnit, propertydeal.CAMCharge, propertydeal.Insurance, propertydeal.Tax, propertydeal.TICharge, propertydeal.TIUnit, propertydeal.FreeRent, propertydeal.FreeRentUnit, outPropertyDealIdValue,propertydeal.Nets,propertydeal.SourceType,propertydeal.BrokerName,propertydeal.BrokerId);

            Int64 PropertyDealIdValue = Convert.ToInt64(outPropertyDealIdValue.Value);


            if (PropertyDealIdValue > 0)
            {
                return PropertyDealIdValue;
            }
            else
                return 0;
        }

        public Int64 PropertyDealDelete(PropertyDealTransaction propertydeal)
        {
            sgl = new SegallEntities();

            //PropertyDeals Delete
            ObjectParameter outPropertyDealIdValue = new ObjectParameter("PropertyDealIdValue", DbType.Int64);
            sgl.usp_PropertyDealsDelete(propertydeal.PropertyDealId, outPropertyDealIdValue);

            Int64 PropertyDealIdValue = Convert.ToInt64(outPropertyDealIdValue.Value);


            if (PropertyDealIdValue > 0)
            {
                return PropertyDealIdValue;
            }
            else
                return 0;
        }

        #endregion
    }
}
