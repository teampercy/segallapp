﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;

namespace SegallBll.DAL
{
    public class PropertyFilterDB
    {
        #region Objects

        SegallEntities sgl;

        #endregion

        #region Properties

        public string PropertyName { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string State { get; set; }
        public string Zipcode { get; set; }
        public decimal? SizeFrom { get; set; }
        public decimal? SizeTo { get; set; }
        public byte? Category { get; set; }
        public decimal? SalesWeeklyFrom { get; set; }
        public decimal? SalesWeeklyTo { get; set; }
        public decimal? SalesMonthlyFrom { get; set; }
        public decimal? SalesMonthlyTo { get; set; }
        public decimal? SalesAnuallyFrom { get; set; }
        public decimal? SalesAnuallyTo { get; set; }                                            
        //public int? SalesPeriodId { get; set; }
        public DateTime? SalesPeriodFrom { get; set; }
        public DateTime? SalesPeriodTo { get; set; }
        public byte? DealType { get; set; }
        public byte? DealSubType { get; set; }
        public decimal? LandSizeFrom { get; set; }
        public decimal? LandSizeTo { get; set; }
        public decimal? BuildingSizeFrom { get; set; }
        public decimal? BuildingSizeTo { get; set; }
        public decimal? RentSizeFrom { get; set; }
        public decimal? RentSizeTo { get; set; }
        public decimal? RentAmountFrom { get; set; }
        public decimal? RentAmountTo { get; set; }
        public string location { get; set; }
        public int? Distance { get; set; }
        public Int64 LoggedInUser { get; set; }
        public Int32 RecPerPage { get; set; }
        public Int32 PageNo { get; set; }
        public Boolean FavoriteOnly { get; set; }

        //public ObjectResult<usp_PropertiesSearch_Result> searchresult { get; set; }

        #endregion

        public object[] PropertySearch(PropertyFilterDB propertyfilter)
        {
            sgl = new SegallEntities();
            object[] searchresult = new object[2];
            try
            {
                string propertyname;
                string city;
                string county;
                string state;
                string zipcode;
                byte? category;
                int? salesperiodid;
                byte? dealtype;
                byte? dealsubtype;
                DateTime? periodFrom = null;
                DateTime? periodTo = null;

                if (!String.IsNullOrEmpty(propertyfilter.PropertyName))
                    propertyname = propertyfilter.PropertyName;
                else
                    propertyname = null;
                if (!String.IsNullOrEmpty(propertyfilter.City))
                    city = propertyfilter.City;
                else
                    city = null;
                if (!String.IsNullOrEmpty(propertyfilter.County))
                    county = propertyfilter.County;
                else
                    county = null;
                if (!String.IsNullOrEmpty(propertyfilter.State))
                    state = propertyfilter.State;
                else
                    state = null;
                if (!String.IsNullOrEmpty(propertyfilter.Zipcode))
                    zipcode = propertyfilter.Zipcode;
                else
                    zipcode = null;
                if (!String.IsNullOrEmpty(propertyfilter.Zipcode))
                    zipcode = propertyfilter.Zipcode;
                else
                    zipcode = null;
                if (propertyfilter.Category != 0)
                    category = propertyfilter.Category;
                else
                    category = null;
                //if (propertyfilter.SalesPeriodId != 0)
                //    salesperiodid = propertyfilter.SalesPeriodId;
                //else
                //    salesperiodid = null;

                if (propertyfilter.SalesPeriodFrom != null)
                    periodFrom = propertyfilter.SalesPeriodFrom;

                if (propertyfilter.SalesPeriodTo != null)
                    periodTo = propertyfilter.SalesPeriodTo;

                if (propertyfilter.DealType != 0)
                    dealtype = propertyfilter.DealType;
                else
                    dealtype = null;
                if (propertyfilter.DealSubType != 0)
                    dealsubtype = propertyfilter.DealSubType;
                else
                    dealsubtype = null;
                searchresult[0] = sgl.usp_PropertiesSearch(propertyfilter.LoggedInUser, propertyname, city, county, state, zipcode, propertyfilter.SizeFrom, propertyfilter.SizeTo, category, propertyfilter.SalesWeeklyFrom, propertyfilter.SalesWeeklyTo, propertyfilter.SalesMonthlyFrom, propertyfilter.SalesMonthlyTo, propertyfilter.SalesAnuallyFrom, propertyfilter.SalesAnuallyTo, periodFrom, periodTo,  dealtype, dealsubtype, propertyfilter.LandSizeFrom, propertyfilter.LandSizeTo, propertyfilter.BuildingSizeFrom, propertyfilter.BuildingSizeTo,propertyfilter.RentSizeFrom,propertyfilter.RentSizeTo,propertyfilter.RentAmountFrom,propertyfilter.RentAmountTo, propertyfilter.location, Distance, 1, 100, FavoriteOnly).ToList();
                searchresult[1] = sgl.usp_GetTypeValues().ToList();
            }
            catch (Exception ex)
            { 
            
            }
            return searchresult;

        }

        
        public object[] GetMapResults()
        {
            sgl = new SegallEntities();
            //List<usp_PropertiesSearch_Result> propertySearchResult = new List<usp_PropertiesSearch_Result>();
            object[] propertySearchResult = new object[1];
            try
            {
                propertySearchResult[0] = sgl.usp_PropertiesSearch(LoggedInUser, PropertyName, City, County, State, Zipcode, SizeFrom, SizeTo, Category, SalesWeeklyFrom, SalesWeeklyTo, SalesMonthlyFrom, SalesMonthlyTo, SalesAnuallyFrom, SalesAnuallyTo, SalesPeriodFrom, SalesPeriodTo, DealType, DealSubType, LandSizeFrom, LandSizeTo, BuildingSizeFrom, BuildingSizeTo, RentSizeFrom, RentSizeTo, RentAmountFrom, RentAmountTo, location, Distance, 1, 1000, FavoriteOnly).ToList();

            }
            catch (Exception ex)
            {

            }
            return propertySearchResult;

        }

        public List<PropertySearchResult> GetPropertySearchResults()
        {
            sgl = new SegallEntities();
            //List<usp_PropertiesSearch_Result> propertySearchResult = new List<usp_PropertiesSearch_Result>();
            ObjectResult<usp_PropertiesSearch_Result> propertySearchResult;
            List<PropertySearchResult> properties = new List<PropertySearchResult>();
            try
            {
                //propertySearchResult = sgl.usp_PropertiesSearch(LoggedInUser, PropertyName, City, County, State, Zipcode, SizeFrom, SizeTo, Category, SalesWeeklyFrom, SalesWeeklyTo, SalesMonthlyFrom, SalesMonthlyTo, SalesAnuallyFrom, SalesAnuallyTo, SalesPeriodFrom, SalesPeriodTo, DealType, DealSubType, LandSizeFrom, LandSizeTo, BuildingSizeFrom, BuildingSizeTo, RentSizeFrom, RentSizeTo, RentAmountFrom, RentAmountTo, location, Distance, PageNo, RecPerPage, FavoriteOnly);
                var propertySearchResults = sgl.usp_PropertiesSearch(LoggedInUser, PropertyName, City, County, State, Zipcode, SizeFrom, SizeTo, Category, SalesWeeklyFrom, SalesWeeklyTo, SalesMonthlyFrom, SalesMonthlyTo, SalesAnuallyFrom, SalesAnuallyTo, SalesPeriodFrom, SalesPeriodTo, DealType, DealSubType, LandSizeFrom, LandSizeTo, BuildingSizeFrom, BuildingSizeTo, RentSizeFrom, RentSizeTo, RentAmountFrom, RentAmountTo, location, Distance, PageNo, RecPerPage, FavoriteOnly).ToList();
                foreach (var prop in propertySearchResults)
                {
                    PropertySearchResult property = new PropertySearchResult();
                    property.propertySearch = prop;
                    property.propertyDeal = sgl.usp_GetPropertyDeals(Convert.ToInt64(prop.PropertyId)).ToList();
                    property.propertySalesVolume = sgl.usp_GetPropertySalesVolume(Convert.ToInt64(prop.PropertyId)).ToList();
                    properties.Add(property); 

                }

            }
            catch (Exception ex)
            {

            }
            return properties;

        }


    }
}
