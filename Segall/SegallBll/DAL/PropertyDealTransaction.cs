﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SegallBll;

namespace SegallBll.DAL
{
    public class PropertyDealTransaction
    {
        #region Properties

        //PropertyDeal PropertyDeal = new PropertyDeal();
        public long PropertyDealId { get; set; }
        public Nullable<long> PropertyId { get; set; }
        public Nullable<byte> DealType { get; set; }
        public Nullable<byte> DealSubType { get; set; }
        public Nullable<decimal> LandSize { get; set; }
        public Nullable<byte> LandSizeUnit { get; set; }
        public Nullable<decimal> BuildingSize { get; set; }
        public Nullable<byte> BuildingSizeUnit { get; set; }
        public Nullable<decimal> ConsiderationAmt { get; set; }
        public Nullable<decimal> RentedSize { get; set; }
        public Nullable<decimal> RentAmount { get; set; }
        public Nullable<byte> RentUnit { get; set; }
        public Nullable<decimal> CAMCharge { get; set; }
        public Nullable<decimal> Insurance { get; set; }
        public Nullable<decimal> Tax { get; set; }
        public Nullable<decimal> TICharge { get; set; }
        public Nullable<byte> TIUnit { get; set; }
        public Nullable<decimal> FreeRent { get; set; }
        public Nullable<byte> FreeRentUnit { get; set; }
        public Nullable<byte> TransactionType { get; set; }
        public Nullable<decimal> Nets { get; set; }
        public string SourceType { get; set; }
        public string BrokerName { get; set; }
        public Nullable<long> BrokerId { get; set; }
        #endregion


    }
}
