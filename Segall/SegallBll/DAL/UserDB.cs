﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Objects;

namespace SegallBll.DAL
{
    public class UserDB
    {
        #region variable

        SegallEntities sgl;

        #endregion

        #region Properties
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Company { get; set; }
        public Int64 UserId { get; set; }
        public bool IsActive { get; set; }
        public Int32 TransactionType { get; set; }
        public bool CanAddDeleteUser { get; set; }
        public bool CanViewRecords { get; set; }
        public bool CanAddRecords { get; set; }
        public bool CanDeleteRecords { get; set; }
        public bool CanEditRecords { get; set; }
        public bool CanViewAllRecords { get; set; }

        #endregion



        public List<usp_GetUserAll_Result> GetUserAll()
        {
            sgl = new SegallEntities();
            List<usp_GetUserAll_Result> getUserAll = new List<usp_GetUserAll_Result>();
            try
            {
                getUserAll = sgl.usp_GetUserAll().ToList();
            }
            catch (Exception ex)
            { }
            return getUserAll;
        }


        public void UserInsert(UserDB user)
        {
            sgl = new SegallEntities();
            try
            {
                ObjectParameter ObjUserid = new ObjectParameter("UserId", DbType.UInt64);
                sgl.usp_UsersInsert(ObjUserid, user.Email, user.Password, 2, DateTime.Now, null, null, user.FirstName, user.LastName, user.Company, user.IsActive, user.CanAddDeleteUser, user.CanViewRecords, user.CanAddRecords, user.CanDeleteRecords, user.CanEditRecords, user.CanViewAllRecords);
            }
            catch (Exception ex)
            { }
        }

        public void UserUpdate(UserDB user)
        {
            sgl = new SegallEntities();

            try
            {
                sgl.usp_UsersUpdate(user.Email,user.Password,user.FirstName,user.LastName,user.Company, user.UserId, user.CanAddDeleteUser, user.CanViewRecords, user.CanAddRecords, user.CanDeleteRecords, user.CanEditRecords, user.CanViewAllRecords);
            }
            catch (Exception ex)
            { }
        }

        public void UserDelete(UserDB user)
        {
            sgl = new SegallEntities();
            try
            {
                sgl.usp_UsersDelete(user.UserId);
            }
            catch (Exception ex)
            { }
        }

    }
}
