//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SegallBll
{
    using System;
    
    public partial class usp_GetUsers_Result
    {
        public long UserId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Name { get; set; }
        public bool SalesVolume { get; set; }
        public bool RentInformation { get; set; }
        public long PropertyId { get; set; }
        public string PropertyName { get; set; }
    }
}
