﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SegallBll.ViewModel;

namespace Segall.Controllers
{
    public class EmailController : Controller
    {
        //
        // GET: /Email/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult SendMail(string attachmentPath, string ReportName)
        {
            EmailViewModel Viewmodel = new EmailViewModel();
            Viewmodel.AttachmentPath = attachmentPath;
            Viewmodel.ReportName = ReportName;
            return View(Viewmodel);
        }

        [HttpPost]
        public ActionResult SendMail(EmailViewModel ViewModel)
        {
            List<string> ToList = new List<string>();
            List<string> AttachemtpathList = new List<string>();
            for (int i = 1; i < ViewModel.Emailto.Length; )
            {
                if (ViewModel.Emailto.IndexOf(",") != -1)
                {
                    ToList.Add(ViewModel.Emailto.Substring(0, ViewModel.Emailto.IndexOf(",")));
                    ViewModel.Emailto = ViewModel.Emailto.Remove(0, ViewModel.Emailto.IndexOf(",") + 1);
                }
                else
                {
                    ToList.Add(ViewModel.Emailto.Trim());
                    ViewModel.Emailto = "";
                }
            }
            //ViewModel.AttachmentPath = Server.MapPath(ConfigurationManager.AppSettings["AttachmentPath"].ToString() + "Chipotle Pikesville Search.pdf");
            AttachemtpathList.Add(ViewModel.AttachmentPath);
            Utility.Utility.sendEmail(ToList, ViewModel.EmailFrom, ViewModel.subject, ViewModel.Body, AttachemtpathList);

            return RedirectToAction("MailSent");
        }

        public ActionResult MailSent()
        {
            return View();
        }
    }
}
