﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using SegallBll;
using SegallBll.ViewModel;

namespace Segall.Controllers
{
    public class AccountController : Controller
    {
        //
        // GET: /Account/

        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }


        [AllowAnonymous]
        public ActionResult AdminLogin()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel ViewModel)
        {
            //string a = "0";
            //int i = 1 / Convert.ToInt32(a);
            //try
            //{
             
              

            //}
            //catch (Exception)
            //{
                
            //    throw;
            //}

          
            if ((ViewModel.user.Email != "" && ViewModel.user.password != "") && (ViewModel.user.Email != null && ViewModel.user.password != null))
            {
                var client = new HttpClient();
                var UserUrl = Url.RouteUrl(
                    "DefaultApi",
                    new { httproute = "", controller = "users", email = ViewModel.user.Email, password = ViewModel.user.password },
                    Request.Url.Scheme
                    );
                var user = client.GetAsync(UserUrl)
                    .ContinueWith((userWithResponse) =>
                    {

                        var response = userWithResponse.Result;
                        response.EnsureSuccessStatusCode();
                        ViewModel.user = response.Content.ReadAsAsync<User>().Result;
                        userWithResponse.Wait();
                    });
                user.Wait();
                if (ViewModel.user.Email != null)
                {
                    Session["LoggedInUser"] = ViewModel.user;
                    FormsAuthentication.RedirectFromLoginPage(ViewModel.user.Email, true);                   
                    return RedirectToAction("Home", "Home");
                }
                else
                {                   
                    ModelState.AddModelError("", "Invalid User Name or Password");
                }
            }
            else
            {                
                ModelState.AddModelError("", "Invalid User Name or Password");
            }
            return View(ViewModel);
        }



        [HttpPost]
        public ActionResult AdminLogin(LoginViewModel ViewModel)
        {
            if ((ViewModel.user.Email != "" && ViewModel.user.password != "") && (ViewModel.user.Email != null && ViewModel.user.password != null))
            {
                var client = new HttpClient();
                var UserUrl = Url.RouteUrl(
                    "DefaultApi",
                    new { httproute = "", controller = "users", email = ViewModel.user.Email, password = ViewModel.user.password },
                    Request.Url.Scheme
                    );
                var user = client.GetAsync(UserUrl)
                    .ContinueWith((userWithResponse) =>
                    {

                        var response = userWithResponse.Result;
                        response.EnsureSuccessStatusCode();
                        ViewModel.user = response.Content.ReadAsAsync<User>().Result;
                        userWithResponse.Wait();
                    });
                user.Wait();
                if (ViewModel.user.Email != null)
                {
                    Session["LoggedInUser"] = ViewModel.user;
                    if (ViewModel.user.UserRoleId == 1)
                    {
                        FormsAuthentication.RedirectFromLoginPage(ViewModel.user.Email, true);
                        return RedirectToAction("SiteAdministration", "Administration");
                    }
                    else { ModelState.AddModelError("", ViewModel.user.Firstname + " " + ViewModel.user.Lastname + " is not authorize for Admin Module"); }
                }
                else
                {
                    ModelState.AddModelError("", "Invalid User Name or Password");
                }
            }
            else
            {
                ModelState.AddModelError("", "Invalid User Name or Password");
            }
            return View(ViewModel);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Login");
        }


        
    }
}
