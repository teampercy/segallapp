﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using SegallBll;
using SegallBll.DAL;
using SegallBll.ViewModel;

namespace Segall.Controllers
{
    public class AdministrationController : Controller
    {
        //
        // GET: /Administration/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SiteAdministration()
        {
            LoginViewModel ViewModel = new LoginViewModel();
            var client = new HttpClient();
            var UserUrl = Url.RouteUrl(
                "DefaultApi",
                new { httproute = "", controller = "Users" },
                Request.Url.Scheme
                );
            var user = client.GetAsync(UserUrl)
                .ContinueWith((userWithResponse) =>
                {
                    var response = userWithResponse.Result;
                    response.EnsureSuccessStatusCode();
                    ViewModel.userall = response.Content.ReadAsAsync<List<usp_GetUserAll_Result>>().Result;
                    userWithResponse.Wait();
                });
            user.Wait();
            return View(ViewModel);
        }

        public ActionResult SetUserDetails(Int64 UserId, bool CanAddDeleteUser, bool CanViewRecords, bool CanAddRecords, bool CanDeleteRecords, bool CanEditRecords, bool CanViewAllRecords)
        {
            LoginViewModel viewModel = new LoginViewModel();
            var client = new HttpClient();
            var UserUrl = Url.RouteUrl(
                  "DefaultApi",
                  new { httproute = "", controller = "Users", id = UserId },
                  Request.Url.Scheme
                  );
            var propertydetail = client.GetAsync(UserUrl)
                .ContinueWith((userWithResponse) =>
                {

                    var response = userWithResponse.Result;
                    response.EnsureSuccessStatusCode();
                    viewModel.user = response.Content.ReadAsAsync<User>().Result;
                    userWithResponse.Wait();
                });
            propertydetail.Wait();

            return Json("1");
        }

        //public ActionResult SaveUser(string FirstName, string LastName, string Email, string Password, string Company, Int64 UserId, byte TransactionType, bool CanAddDeleteUser, bool CanViewRecords, bool CanAddRecords, bool CanDeleteRecords, bool CanEditRecords, bool CanViewAllRecords)
        //{
        //    LoginViewModel viewModel = new LoginViewModel();
        //    var client = new HttpClient();
        //    if (UserId > 0)
        //    {
        //        var UserUrl = Url.RouteUrl(
        //           "DefaultApi",
        //           new { httproute = "", controller = "Users", FirstName = FirstName, LastName = LastName, Email = Email, Password = Password, Company = Company, UserId = UserId, TransactionType = TransactionType, CanAddDeleteUser = CanAddDeleteUser, CanViewRecords = CanViewRecords, CanAddRecords = CanAddRecords, CanDeleteRecords = CanDeleteRecords, CanEditRecords = CanEditRecords, CanViewAllRecords = CanViewAllRecords },
        //           Request.Url.Scheme
        //           );
        //        var user = client.PostAsJsonAsync(UserUrl, "")
        //            .ContinueWith((userWithResponse) =>
        //            {
        //                var response = userWithResponse.Result;
        //                response.EnsureSuccessStatusCode();
        //                //ViewModel.userall = response.Content.ReadAsAsync<List<usp_GetUserAll_Result>>().Result;
        //                userWithResponse.Wait();
        //            });
        //        user.Wait();

        //    }
        //    else
        //    {
        //        var UserUrl = Url.RouteUrl(
        //           "DefaultApi",
        //           new { httproute = "", controller = "Users", FirstName = FirstName, LastName = LastName, Email = Email, Password = Password, Company = Company, UserId = UserId, TransactionType = TransactionType },
        //           Request.Url.Scheme
        //           );
        //        var user = client.PostAsJsonAsync(UserUrl, "")
        //            .ContinueWith((userWithResponse) =>
        //            {
        //                var response = userWithResponse.Result;
        //                response.EnsureSuccessStatusCode();
        //                //ViewModel.userall = response.Content.ReadAsAsync<List<usp_GetUserAll_Result>>().Result;
        //                userWithResponse.Wait();
        //            });
        //        user.Wait();
        //    }


        //    return Json("1");
        //}


        //public ActionResult DeleteUser(string FirstName, string LastName, string Email, string Password, string Company, Int64 UserId, byte TransactionType)
        //{

        //    var client = new HttpClient();
        //    var UserUrl = Url.RouteUrl(
        //        "DefaultApi",
        //        new { httproute = "", controller = "Users", FirstName = FirstName, LastName = LastName, Email = Email, Password = Password, Company = Company, UserId = UserId, TransactionType = TransactionType },
        //        Request.Url.Scheme
        //        );
        //    var user = client.PostAsJsonAsync(UserUrl, "")
        //        .ContinueWith((userWithResponse) =>
        //        {
        //            var response = userWithResponse.Result;
        //            response.EnsureSuccessStatusCode();
        //            //ViewModel.userall = response.Content.ReadAsAsync<List<usp_GetUserAll_Result>>().Result;
        //            userWithResponse.Wait();
        //        });
        //    user.Wait();
        //    return Json("1");
        //}

        public ActionResult SaveUserNew(UserDB user)
        {

            LoginViewModel viewModel = new LoginViewModel();
            var client = new HttpClient();
            var UserUrl = Url.RouteUrl(
               "DefaultApi",
               new { httproute = "", controller = "Users" },
               Request.Url.Scheme
               );
            var userdata = client.PostAsJsonAsync(UserUrl, user)
                .ContinueWith((userWithResponse) =>
                {
                    var response = userWithResponse.Result;
                    response.EnsureSuccessStatusCode();
                    //ViewModel.userall = response.Content.ReadAsAsync<List<usp_GetUserAll_Result>>().Result;
                    userWithResponse.Wait();
                });
            userdata.Wait();
            return Json("1");
        }

    }
}
