﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Mvc;
using SegallBll.ViewModel;
using SegallBll;
using SegallBll.DAL;
using SegallBll.Custom_Class;

namespace Segall.Controllers
{
    public class PropertyDetailController : CustomController
    {
        //
        // GET: /NewEntry/
        SegallEntities sgl = new SegallEntities();
        public List<PropertyBasic> pb { get; set; }

        public ActionResult Index()
        {
            return View();
        }

        //public ActionResult PropertyDemoNewEntry(List<PropertyDeal> lstPropertyDeal, FormCollection form)
        //{

        //    SegallBll.User LoggedInUser = (User)Session["LoggedInUser"];
        //    form["hdnLoggedInUser"] = LoggedInUser.UserId.ToString();

        //    PropertyDetailViewModel viewmodel = new PropertyDetailViewModel();

        //    viewmodel.LoggedInUser = Convert.ToInt64(form["hdnLoggedInUser"]);
        //    if (TempData["demoNewEntry"] != null)
        //    {

        //        viewmodel.lstPropertyDeal.Add((SegallBll.PropertyDeal)TempData["demoNewEntry"]);

        //    }

        //    return View(viewmodel);
        //}

        public ActionResult PropertyFilters(string location)
        {
            string locationvalue = location;
            SegallBll.User LoggedInUser = (User)Session["LoggedInUser"];

            PropertyDetailViewModel viewmodel = new PropertyDetailViewModel();
            viewmodel.LoggedInUser = LoggedInUser.UserId;
            viewmodel.Location = location;
            return View(viewmodel);
        }

        [HttpPost]
        public ActionResult EncryptPropertyId(string propertyid ="")
        {
            string propertyid_encr = Encryption.encriptString(propertyid);
            return Json(propertyid_encr,JsonRequestBehavior.AllowGet);
        }

        //public ActionResult PropertyNewEntry(Int64 propertyid = 0, decimal Latitude = 0, decimal Longitude = 0, string ct = "", string st = "", string zp = "", string co="")                
        public ActionResult PropertyNewEntry(string propertyid = "048", decimal Latitude = 0, decimal Longitude = 0, string ct = "", string st = "", string zp = "", string co="")                
        {
            //048 is encrypted value of 0
            Int64 propertyid_decr = Convert.ToInt64(Encryption.decryptString(propertyid));                
            PropertyDetailViewModel viewModel = new PropertyDetailViewModel();
            SegallBll.User LoggedInUser = (User)Session["LoggedInUser"];
            if (propertyid_decr > 0)
            {
                PropertyDetailDB propDetail = new PropertyDetailDB();
                var client = new HttpClient();
                var UserUrl = Url.RouteUrl(
                    "DefaultApi",
                    new { httproute = "", controller = "propertybasic", PropertyId = propertyid_decr },
                    Request.Url.Scheme
                    );
                var propertydetail = client.GetAsync(UserUrl)
                    .ContinueWith((userWithResponse) =>
                    {
                        var response = userWithResponse.Result;
                        response.EnsureSuccessStatusCode();
                        viewModel.propertydetail = response.Content.ReadAsAsync<PropertyDetailDB>().Result;
                        //propDetail = response.Content.ReadAsAsync<PropertyDetailDB>().Result;
                        userWithResponse.Wait();
                    });
                propertydetail.Wait();

               
                //viewmodel.propertybasic  = propDetail.propertybasicresult[0]; 


                if (viewModel.propertydetail.propertybasicresult != null)
                {
                    foreach (var basic in viewModel.propertydetail.propertybasicresult)
                    {
                        viewModel.PropertyId = basic.PropertyId;
                        viewModel.propertybasic.PropertyName = basic.PropertyName;
                        viewModel.propertybasic.Size = basic.Size;
                        viewModel.propertybasic.SizeUnit = basic.SizeUnit;
                        viewModel.propertybasic.Address = basic.Address;
                        viewModel.propertybasic.City = basic.City;
                        viewModel.propertybasic.County = basic.County;
                        viewModel.propertybasic.State = basic.State;
                        viewModel.propertybasic.Zipcode = basic.Zipcode;
                        viewModel.propertybasic.Latitude = basic.Latitude;
                        viewModel.propertybasic.Longitude = basic.Longitude;
                        viewModel.propertybasic.Center = basic.Center;
                        viewModel.propertybasic.GLA = basic.GLA;
                        viewModel.propertybasic.Land = basic.Land;
                        viewModel.propertybasic.category = basic.category;
                        viewModel.propertybasic.Zoning = basic.Zoning;
                        viewModel.propertybasic.PropertyAddedBy = basic.PropertyAddedBy;
                        viewModel.propertybasic.PropertyAddedDate = basic.PropertyAddedDate;
                        viewModel.LoggedInUser = LoggedInUser.UserId;
                        viewModel.propertybasic.Notes = basic.Notes;
                        viewModel.propertybasic.Tenant = basic.Tenant;
                    }
                }
                
                if (viewModel.propertydetail.propertysalesvolumeresult != null)
                {
                    foreach (var salesvolume in viewModel.propertydetail.propertysalesvolumeresult)
                    {                        
                        viewModel.propertySalesVolume.PropertyId = salesvolume.PropertyId;
                        viewModel.SalesVolumeId = salesvolume.SalesVolumeId;
                        viewModel.propertySalesVolume.PeriodFrom = salesvolume.PeriodFrom;
                        viewModel.propertySalesVolume.PeriodTo = salesvolume.PeriodTo;
                        viewModel.propertySalesVolume.SalesWeekly = salesvolume.SalesWeekly;
                        viewModel.propertySalesVolume.SalesMonthly = salesvolume.SalesMonthly;
                        viewModel.propertySalesVolume.SalesAnnually = salesvolume.SalesAnnually;
                        viewModel.propertySalesVolume.VolumePerSF = salesvolume.VolumePerSF;
                        viewModel.propertySalesVolume.PeriodYear = salesvolume.PeriodYear;
                        viewModel.propertySalesVolume.SourceType = salesvolume.SourceType;
                        viewModel.propertySalesVolume.BrokerName = salesvolume.BrokerName;
                        viewModel.propertySalesVolume.BrokerId = salesvolume.BrokerId;
                    }
                }
                if (viewModel.propertydetail.propertydealsresult != null)
                {
                    foreach (var deals in viewModel.propertydetail.propertydealsresult)
                    {
                        viewModel.PropertyDealId = deals.PropertyDealId;
                        viewModel.propertydeal.DealType = deals.DealTypeValue;
                        viewModel.propertydeal.DealSubType = deals.DealSubTypeValue;
                        viewModel.propertydeal.LandSize = Convert.ToDecimal(deals.LandSize);
                        viewModel.propertydeal.LandSizeUnit = Convert.ToByte(deals.LandSizeUnitValue);
                        viewModel.propertydeal.BuildingSize = deals.BuildingSize;
                        viewModel.propertydeal.BuildingSizeUnit = Convert.ToByte(deals.BuildingSizeUnitValue);
                        viewModel.propertydeal.ConsiderationAmt = deals.ConsiderationAmt;
                        viewModel.propertydeal.RentedSize = deals.RentedSize;
                        viewModel.propertydeal.RentAmount = deals.RentAmount;
                        viewModel.propertydeal.RentUnit = Convert.ToByte(deals.RentUnitValue);
                        viewModel.propertydeal.CAMCharge = deals.Camcharge;
                        viewModel.propertydeal.Insurance = deals.Insurance;
                        viewModel.propertydeal.Tax = deals.Tax;
                        viewModel.propertydeal.TICharge = deals.TICharge;
                        viewModel.propertydeal.TIUnit = Convert.ToByte(deals.TIUnitValue);
                        viewModel.propertydeal.FreeRent = deals.FreeRent;
                        viewModel.propertydeal.FreeRentUnit = Convert.ToByte(deals.FreeRentUnitValue);
                        viewModel.propertydeal.Nets = deals.Nets;
                        viewModel.propertydeal.SourceType = deals.SourceType;
                        viewModel.propertydeal.BrokerName = deals.BrokerName;
                        viewModel.propertydeal.BrokerId = deals.BrokerId;
                    }
                }



                TempData["PropertyDetailViewModel"] = viewModel;

             }
            else
            {
                
                viewModel.PropertyId = 0;
                viewModel.SalesVolumeId = 0;
                viewModel.LoggedInUser = LoggedInUser.UserId;

                //Below If is introduced to add latitude and logitude if the user drops the new entry image on the map.
                    if (Latitude != 0 && Longitude != 0)
                    {
                        viewModel.propertybasic.Latitude = Latitude;
                        viewModel.propertybasic.Longitude = Longitude;
                        viewModel.propertybasic.City = ct;
                        viewModel.propertybasic.State = st;
                        viewModel.propertybasic.Zipcode = zp;
                        viewModel.propertybasic.County = co;
                    }
         }
            if (viewModel.propertySalesVolume.BrokerId ==null || viewModel.propertySalesVolume.BrokerId <= 0)
            viewModel.propertySalesVolume.BrokerId = viewModel.LoggedInUser;

            if (viewModel.propertydeal.BrokerId == null || viewModel.propertydeal.BrokerId <= 0)
                viewModel.propertydeal.BrokerId = viewModel.LoggedInUser;

            return View(viewModel);          
        }

        public ActionResult PropertyDealsData(Int64 propertyid)
        {
            PropertyDetailViewModel viewModel = new PropertyDetailViewModel();
            var client = new HttpClient();
            var UserUrl = Url.RouteUrl(
                "DefaultApi",
                new { httproute = "", controller = "propertybasic", PropertyId = propertyid },
                Request.Url.Scheme
                );
            var propertydetail = client.GetAsync(UserUrl)
                .ContinueWith((userWithResponse) =>
                {

                    var response = userWithResponse.Result;
                    response.EnsureSuccessStatusCode();
                    viewModel.propertydetail = response.Content.ReadAsAsync<PropertyDetailDB>().Result;
                    userWithResponse.Wait();
                });
            propertydetail.Wait();
            viewModel = new PropertyDetailViewModel(viewModel);
            TempData["PropertyDetailViewModel"] = viewModel;
            
            return Json(RenderPartialViewToString("_PropertyDealsList", viewModel));
        }

        public ActionResult SalesVolumeData(Int64 propertyid)
        {
            PropertyDetailViewModel viewModel = new PropertyDetailViewModel();
            var client = new HttpClient();
            var UserUrl = Url.RouteUrl(
                "DefaultApi",
                new { httproute = "", controller = "propertybasic", PropertyId = propertyid },
                Request.Url.Scheme
                );
            var propertydetail = client.GetAsync(UserUrl)
                .ContinueWith((userWithResponse) =>
                {

                    var response = userWithResponse.Result;
                    response.EnsureSuccessStatusCode();
                    viewModel.propertydetail = response.Content.ReadAsAsync<PropertyDetailDB>().Result;
                    userWithResponse.Wait();
                });
            propertydetail.Wait();
            viewModel = new PropertyDetailViewModel(viewModel);
            TempData["PropertyDetailViewModel"] = viewModel;

            return Json(RenderPartialViewToString("_SalesVolumeList", viewModel));
        }

        public ActionResult ShowPropertyOnMap(Int64 propertyid)
        {

            PropertyInfoViewModel Viewmodel = new PropertyInfoViewModel();
            SegallEntities sgl = new SegallEntities();
            SegallBll.User LoggedInUser = new SegallBll.User();
            LoggedInUser = (SegallBll.User)Session["LoggedInUser"];
            
            var client = new HttpClient();
            var UserUrl = Url.RouteUrl(
                "DefaultApi",
                new { httproute = "", controller = "PropertyBasic", PropertyInfo = propertyid },
                Request.Url.Scheme
                );
            var report = client.GetAsync(UserUrl)
                .ContinueWith((userWithResponse) =>
                {

                    var response = userWithResponse.Result;
                    response.EnsureSuccessStatusCode();
                    Viewmodel.propertyInfoDetails = response.Content.ReadAsAsync<PropertyInfoDetails>().Result;
                    userWithResponse.Wait();
                });
            report.Wait();
            PropertyFilterDB filter = new PropertyFilterDB();
            filter.location = Viewmodel.propertyInfoDetails.propertyInfo.PropertyDesc;
            filter.LoggedInUser = LoggedInUser.UserId;
            filter.Distance = 0;
            TempData["propertyFilter"] = filter;
            Session["propertyFilter"] = filter;
            TempData["Location"]=Viewmodel.propertyInfoDetails.propertyInfo.PropertyDesc;
            Session["Location"] = Viewmodel.propertyInfoDetails.propertyInfo.PropertyDesc;
            return RedirectToAction("ShowMap", "SearchMap");
        }

        public ActionResult DeleteProperty(Int64 PropertyId)
        {
            PropertyBasicDB PropertyBasic = new PropertyBasicDB();
            string Message = PropertyBasic.deletePropertyBasic(PropertyId);
            TempData["Location"] = Session["Location"];
            return Json(Message);
        }
        //public ActionResult LoadSalesVolume()
        //{
        //    return View("PropertyNewEntry",);
        //}
        public ActionResult SaveSizeAndVolume(Int64 propertyid, decimal sizeData)
        {
            int result = 0;
            decimal Sizes = Convert.ToDecimal(sizeData);           


            //String HtmlStringPropertyInfo = RenderPartialViewToString("_PropertyInfo", Viewmodel);
            //return Json(HtmlStringPropertyInfo);


            var client = new HttpClient();
            var UserUrl = Url.RouteUrl(
               "DefaultApi",
               new { httproute = "", controller = "PropertyBasic/UpdateSizeAndVolume", PropertyId = propertyid, size = Sizes },
               Request.Url.Scheme
               );
            var userdata = client.PostAsJsonAsync(UserUrl, "")
                .ContinueWith((userWithResponse) =>
                {
                    var response = userWithResponse.Result;
                    response.EnsureSuccessStatusCode();
                    //ViewModel.userall = response.Content.ReadAsAsync<List<usp_GetUserAll_Result>>().Result;
                    userWithResponse.Wait();
                });
            userdata.Wait();
            return Json("1");


        }
    }
}
