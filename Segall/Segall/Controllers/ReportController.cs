﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using Microsoft.Reporting.WebForms;
using SegallBll.DAL;
using SegallBll;
using SegallBll.ViewModel;

namespace Segall.Controllers
{
    public class ReportController : Controller
    {
        //
        // GET: /Report/

        public ActionResult Index()
        {
            string welcomeMessage = "Welcome to Northwind Traders. Enjoy the view!";
            return View((object)welcomeMessage);
        }

        public ActionResult Report()
        {
            string PropertyName = "ANY";
            string City = "ANY";
            string State = "ANY";
            string County = "ANY";
            string Zipcode = "ANY";
            string Category = "ANY";
            string Sales = "ANY";
            string Period = "ANY";
            string Rent = "ANY";
            string Radius = "ANY";
            string Location;
            #region Filters
            //if (Session["FavoutiteOnly"] == null)
            //{
            //    Session["FavoutiteOnly"] = favouriteOnly;
            //}
            //else
            //{ favouriteOnly = Convert.ToBoolean(Session["FavoutiteOnly"]); }
            if (Session["propertyFilter"] != null)
            {
                TempData["propertyFilter"] = Session["propertyFilter"];
            }
            if (Session["Location"] != null)
            {
                TempData["Location"] = Session["Location"];
            }
            SegallBll.User LoggedInUser = new SegallBll.User();
            LoggedInUser = (SegallBll.User)Session["LoggedInUser"];
            #endregion

            ReportViewModel ViewModel = new ReportViewModel();
            if (TempData["Location"] != null)
            {
                Location = TempData["Location"].ToString();
            }
            if (TempData["propertyFilter"] != null)
            {
                ViewModel.propertyFilter = (PropertyFilterDB)TempData["propertyFilter"];
                ViewModel.propertyFilter.LoggedInUser = LoggedInUser.UserId;
                //ViewModel.propertyFilter.FavoriteOnly = favouriteOnly;
                ViewModel.propertyFilter.RecPerPage = 10;
                //ViewModel.propertyFilter.PageNo = PageNo;
            }

            ReportParameter[] parameters = new ReportParameter[5];
            

            if (!String.IsNullOrEmpty(ViewModel.propertyFilter.PropertyName))
            {
                PropertyName = ViewModel.propertyFilter.PropertyName;
                parameters[0] = new ReportParameter("PropertyName", PropertyName);
            }
            else
                parameters[0] = new ReportParameter("PropertyName", "ANY");
            if (!String.IsNullOrEmpty(ViewModel.propertyFilter.City))
            {
                City = ViewModel.propertyFilter.City;
                parameters[1] = new ReportParameter("City", City);
            }
            else
                parameters[1] = new ReportParameter("City", "ANY");
            if (!String.IsNullOrEmpty(ViewModel.propertyFilter.State))
            {
                State = ViewModel.propertyFilter.State;
                parameters[2] = new ReportParameter("State", State);
            }
            else
                parameters[2] = new ReportParameter("State", "ANY");
            if (!String.IsNullOrEmpty(ViewModel.propertyFilter.County))
            {
                County = ViewModel.propertyFilter.County;
                parameters[3] = new ReportParameter("County", County);
            }
            else
                parameters[3] = new ReportParameter("County", "ANY");
            if (!String.IsNullOrEmpty(ViewModel.propertyFilter.Zipcode))
            {
                Zipcode = ViewModel.propertyFilter.Zipcode;
                parameters[4] = new ReportParameter("Zipcode", Zipcode);
            }
            else
                parameters[4] = new ReportParameter("Zipcode", "ANY");
            if (ViewModel.propertyFilter.Category > 0)
            {
                if (ViewModel.propertyFilter.Category == 1)
                    Category = "Retail";
                if (ViewModel.propertyFilter.Category == 2)
                    Category = "Restaurant";
            }
            if (ViewModel.propertyFilter.SalesWeeklyFrom != null || ViewModel.propertyFilter.SalesWeeklyTo != null)
                Sales = ViewModel.propertyFilter.SalesWeeklyFrom + " - " + ViewModel.propertyFilter.SalesWeeklyTo + "weekly";
            else if (ViewModel.propertyFilter.SalesMonthlyFrom != null || ViewModel.propertyFilter.SalesMonthlyTo != null)
                Sales = ViewModel.propertyFilter.SalesMonthlyFrom + " - " + ViewModel.propertyFilter.SalesMonthlyTo + "monthly";
            else if (ViewModel.propertyFilter.SalesAnuallyFrom != null || ViewModel.propertyFilter.SalesAnuallyTo != null)
                Sales = ViewModel.propertyFilter.SalesAnuallyFrom + " - " + ViewModel.propertyFilter.SalesAnuallyTo + "anually";

            if (ViewModel.propertyFilter.SalesPeriodFrom != null || ViewModel.propertyFilter.SalesPeriodTo != null)
                Period = ViewModel.propertyFilter.SalesPeriodFrom + " - " + ViewModel.propertyFilter.SalesPeriodTo;
            if(ViewModel.propertyFilter.RentAmountFrom != null || ViewModel.propertyFilter.RentAmountTo != null)
                Rent = ViewModel.propertyFilter.RentAmountFrom + " - "+ ViewModel.propertyFilter.RentAmountTo;

            if(ViewModel.propertyFilter.Distance != null)
                Radius = ViewModel.propertyFilter.Distance.ToString();

            
            
            
            
            
            
            
            //parameters[5] = new ReportParameter("Category",Category);
            //parameters[6] = new ReportParameter("SalesWeeklyFrom",Sales);
            //parameters[7] = new ReportParameter("SalesPeriodFrom",Period);
            //parameters[8] = new ReportParameter("RentAmountFrom",Rent);
            //parameters[9] = new ReportParameter("Distance",Radius);
            SegallBll.SegallEntities sgl = new SegallBll.SegallEntities();
            IEnumerable<usp_PropertiesSearch_Result> propertysearch = 
            sgl.usp_PropertiesSearch(LoggedInUser.UserId,ViewModel.propertyFilter.PropertyName,ViewModel.propertyFilter.City,ViewModel.propertyFilter.County,ViewModel.propertyFilter.State,ViewModel.propertyFilter.Zipcode,ViewModel.propertyFilter.SizeFrom,ViewModel.propertyFilter.SizeTo,ViewModel.propertyFilter.Category,ViewModel.propertyFilter.SalesWeeklyFrom,ViewModel.propertyFilter.SalesWeeklyTo,ViewModel.propertyFilter.SalesMonthlyFrom, ViewModel.propertyFilter.SalesMonthlyTo, ViewModel.propertyFilter.SalesAnuallyFrom,ViewModel.propertyFilter.SalesAnuallyTo,ViewModel.propertyFilter.SalesPeriodFrom,ViewModel.propertyFilter.SalesPeriodTo,ViewModel.propertyFilter.DealType,ViewModel.propertyFilter.DealSubType,ViewModel.propertyFilter.LandSizeFrom,ViewModel.propertyFilter.LandSizeTo,ViewModel.propertyFilter.BuildingSizeFrom,ViewModel.propertyFilter.BuildingSizeTo, null, null,ViewModel.propertyFilter.RentAmountFrom,ViewModel.propertyFilter.RentAmountTo, null, null,1, 1000000, null);
            LocalReport localReport = new LocalReport();
            using (System.IO.Stream PropertySalesVolume = System.IO.File.OpenRead(Server.MapPath("~/Report/Rdlcfiles/PropertySalesVolume.rdlc")))
            {
                localReport.LoadSubreportDefinition("PropertySalesVolume", PropertySalesVolume);
            }
            localReport.ReportPath = Server.MapPath("~/Report/Rdlcfiles/PropertiesSearch.rdlc");
            localReport.SetParameters(parameters);
            //IEnumerable<ReportParameter> parameters;
            
            ReportDataSource reportDataSource = new ReportDataSource("PropertiesSearch", propertysearch);

 
            localReport.DataSources.Add(reportDataSource);
            string reportType = "PDF";
            string mimeType;
            string encoding;
            string fileNameExtension;


            string deviceInfo =
            "<DeviceInfo>" +
            "  <OutputFormat>PDF</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.5in</MarginTop>" +
            "  <MarginLeft>0.01in</MarginLeft>" +
            "  <MarginRight>0.01in</MarginRight>" +
            "  <MarginBottom>0.5in</MarginBottom>" +
            "</DeviceInfo>";
 
            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;
 
            //Render the report

            renderedBytes = localReport.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            //Response.AddHeader("content-disposition", "attachment; filename=NorthWindCustomers." + fileNameExtension);
            return File(renderedBytes, mimeType);
        }


        
    }
}
