﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SegallBll.DAL;

namespace Segall.Controllers.API
{
    public class PropertyAccessRightsController : ApiController
    {
        public object GetUsers(Int64 PropertyId)
        {
            object users = new object();
            PropertyAccessRightsDB propertyaccessrightsdb = new PropertyAccessRightsDB();
            users = propertyaccessrightsdb.GetUsers(PropertyId);
            return users;
        }

        public string Post([FromBody] PropertyAccessRightsDB pardb)
        {
            PropertyAccessRightsDB propertyaccessrigthsdb = new PropertyAccessRightsDB();

            if (pardb != null)
            {
                string salesuserid = pardb.SalesUserids;
                Int64 propertyid = pardb.PropertyId;
                //for (int i = 1; i < salesuserid.Length; )
                //{
                //    string id = salesuserid.Substring(0, salesuserid.IndexOf(","));
                //    salesuserid = salesuserid.Substring(salesuserid.IndexOf(",") + 1, (salesuserid.Length - salesuserid.IndexOf(",") - 1));
                //    Int64 SalesUserid = Convert.ToInt64(id);
                    propertyaccessrigthsdb.UpdatePropertyAccessRights(propertyid, salesuserid, "S");

                    string rentuserid = pardb.RentUserids;

                    //for (int i = 1; i < rentuserid.Length; )
                    //{
                    //    string id = rentuserid.Substring(0, rentuserid.IndexOf(","));
                    //    rentuserid = rentuserid.Substring(rentuserid.IndexOf(",") + 1, (rentuserid.Length - rentuserid.IndexOf(",") - 1));
                    //    Int64 RentUserid = Convert.ToInt64(id);
                    propertyaccessrigthsdb.UpdatePropertyAccessRights(propertyid, rentuserid, "R");
                //}
            }
           
            return "Saved Successfully";
        }
    }
}
