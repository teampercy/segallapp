﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SegallBll;
using SegallBll.DAL;
using System.Data.Objects;
using System.Data;

namespace Segall.Controllers.API
{
    public class PropertySalesVolumeController : ApiController
    {
        [HttpPost]
        public Int64 Post(SalesVolumeTransaction salesvolumetransaction)
        {
            Int64 SalesVolumeId;
            SalesVolumeDB salesvolumedb = new SalesVolumeDB();
            if (salesvolumetransaction.SalesVolumeId > 0 && salesvolumetransaction.TransactionType == 2)
            {
                SalesVolumeId = salesvolumedb.SalesVolumeUpdate(salesvolumetransaction);
                return SalesVolumeId;
            }
            else if (salesvolumetransaction.SalesVolumeId <= 0)
            {
                SalesVolumeId = salesvolumedb.SalesVolumeInsert(salesvolumetransaction);
                return SalesVolumeId;
            }
            else if (salesvolumetransaction.SalesVolumeId > 0 && salesvolumetransaction.TransactionType == 3)
            {
                SalesVolumeId = salesvolumedb.SalesVolumeDelete(salesvolumetransaction);
                return SalesVolumeId;
            }
            else
                return 0;


        }
    }
}
