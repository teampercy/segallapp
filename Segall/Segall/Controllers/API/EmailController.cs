﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Segall.Controllers.API
{
    public class EmailController : ApiController
    {

        public bool GetEmailSend(string to, string from, string subject, string body, string Attachemtpath)
        {
            List<string> ToList = new List<string>();
            List<string> AttachemtpathList = new List<string>();
            for (int i = 1; i < to.Length; )
            {
                if (to.IndexOf(",") != -1)
                {
                    ToList.Add(to.Substring(0, to.IndexOf(",")));
                    to = to.Remove(0, to.IndexOf(",") + 1);
                }
                else
                {
                    ToList.Add(to.Trim());
                    to = "";
                }
            }
            AttachemtpathList.Add(Attachemtpath);
            Utility.Utility.sendEmail(ToList, from, subject, body, AttachemtpathList);
            return true;
        }
    }
}
