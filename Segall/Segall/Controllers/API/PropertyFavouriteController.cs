﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SegallBll;
using SegallBll.DAL;

namespace Segall.Controllers.API
{
    public class PropertyFavouriteController : ApiController
    {
        public string Post(Int64 propertyid, Int64 UserId, string type)
        {
            PropertyFavoriteDB Propertyfavourite = new PropertyFavoriteDB();
            string Message = "";
            if (type.ToUpper() == "INSERT")
            {
                Propertyfavourite.InsertFavourite(propertyid, UserId);
                Message = "Inserted Successfully";
            }
            else if (type.ToUpper() == "DELETE")
            {
                Propertyfavourite.DeleteFavourite(propertyid, UserId);
                Message = "Deleted Successfully";
            }
            else if (type.ToUpper() == "DELETEALL")
            {
                Propertyfavourite.DeleteFavouriteALL(UserId);
                Message = "Deleted Successfully";
            }
            return Message;
        }

        public List<usp_GetPropertyFavouriteUserList_Result> GetFavouriteByUserid(Int64 UserId)
        {
            PropertyFavoriteDB Propertyfavourite = new PropertyFavoriteDB();
            List<usp_GetPropertyFavouriteUserList_Result> FavouriteResult = Propertyfavourite.GetFavouriteByUserid(UserId);
            return FavouriteResult;
        }
    }
}
