﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SegallBll.Custom_Class;
using SegallBll.DAL;

namespace Segall.Controllers.API
{
    public class SavedSearchController : ApiController
    {
        PropertySavedSearchDB PropertySavedSearch;
        public object GetSavedSearchAll(Int32 userId)
        {
            object savedsearch = new object();
            PropertySavedSearch = new PropertySavedSearchDB();
            savedsearch = PropertySavedSearch.GetSavedSearchAll(userId);
            return savedsearch;
        }

        public string Post([FromBody] PropertySavedSearchDB savedSearch)
        {
            PropertySavedSearch = new PropertySavedSearchDB();
            if (savedSearch.type.ToUpper() == "DELETE")
            {
               PropertySavedSearch.DeletesavedSearch(Convert.ToInt64(savedSearch.savedSearchId));
            }
            else if (savedSearch.type.ToUpper() == "POST")
            {
                PropertySavedSearch.savedSearch(savedSearch);
            }
            return savedSearch.type + " Successfully";
        }

        //public void Delete([FromBody] CustomClass savedSearch)
        //{
        //    string savedSearchId = savedSearch.savedSearchId;
        //    PropertySavedSearch = new PropertySavedSearchDB();
        //    for (int i = 1; i < savedSearchId.Length;)
        //    {
        //        string id = savedSearchId.Substring(0, savedSearchId.IndexOf(","));
        //        savedSearchId = savedSearchId.Substring(savedSearchId.IndexOf(",") + 1, (savedSearchId.Length - savedSearchId.IndexOf(",") - 1));
        //        PropertySavedSearch.DeletesavedSearch(Convert.ToInt64(id));
        //    }
        //}

       
    }

}
