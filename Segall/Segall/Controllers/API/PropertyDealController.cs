﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SegallBll;
using SegallBll.DAL;
using System.Data.Objects;
using System.Data;

namespace Segall.Controllers.API
{
    public class PropertyDealController : ApiController
    {
        [HttpPost]
        public Int64 Post(PropertyDealTransaction propertydealtransaction)
        {
            Int64 PropertyDealId;
            PropertyDealDB propertydealdb = new PropertyDealDB();
            if (propertydealtransaction.PropertyDealId > 0 && propertydealtransaction.TransactionType == 2)
            {
                PropertyDealId = propertydealdb.PropertyDealUpdate(propertydealtransaction);
                return PropertyDealId;
            }
            else if (propertydealtransaction.PropertyDealId <= 0)
            {
                PropertyDealId = propertydealdb.PropertyDealInsert(propertydealtransaction);
                return PropertyDealId;
            }
            else if (propertydealtransaction.PropertyDealId > 0 && propertydealtransaction.TransactionType == 3)
            {
                PropertyDealId = propertydealdb.PropertyDealDelete(propertydealtransaction);
                return PropertyDealId;
            }
            else
                return 0;
            
            
        }


    }
}
