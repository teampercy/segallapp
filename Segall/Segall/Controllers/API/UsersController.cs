﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SegallBll;
using SegallBll.DAL;
using SegallBll.ViewModel;


namespace Segall.Controllers
{
    public class UsersController : ApiController
    {
        //User[] users = new User[]  
        //{  
        //    new User { Id = 1, UserName = "Tomato Soup",Password="1234", Category = "Groceries", Price = 1 },  
        //    new User { Id = 2, UserName = "Yo-yo", Password="1234", Category = "Toys", Price = 3.75M },  
        //    new User { Id = 3, UserName = "Hammer", Password="1234", Category = "Hardware", Price = 16.99M }  
        //};
        SegallEntities sgl;
        //public IEnumerable<User> GetAllUsers()
        //{
        //    sgl = new SegallEntities();
        //    return sgl.Users;
        //}

        public User GetUserById(int id)
        {
            sgl = new SegallEntities();
            var user = sgl.Users.FirstOrDefault((p) => p.UserId == id);
            if (user == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return user;
        }

        public User GetUserByEmail(string email, string password)
        {
            sgl = new SegallEntities();
            LoginViewModel Viewmodel = new LoginViewModel();
            ObjectParameter outUserStatus = new ObjectParameter("UserStatus", DbType.Int32);
            List<User> user;
            try
            {
               
                sgl.usp_UserValidateCredential(outUserStatus, email, password);
                if (Convert.ToInt32(outUserStatus.Value) == 1)
                {
                    //return sgl.Users.Where(u => u.Email == email).First();
                    Viewmodel.user = sgl.Users.Where(u => u.Email == email ).First();
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
            catch (Exception ex)
            { }

            return Viewmodel.user;
            
        }


        public List<usp_GetUserAll_Result> GetUserAll()
        {
            sgl = new SegallEntities();
            UserDB getUser = new UserDB();
            List<usp_GetUserAll_Result> getUserall = new List<usp_GetUserAll_Result>();
            try
            {
                getUserall = getUser.GetUserAll();
            }
            catch (Exception ex)
            { }
            return getUserall;
        }

        //public void post(string FirstName, string LastName, string Email, string Password, string Company, Int64 UserId, byte TransactionType,bool CanAddDeleteUser,bool CanViewRecords,bool CanAddRecords,bool CanDeleteRecords,bool CanEditRecords,bool CanViewAllRecords)
        //{
        //    UserDB user = new UserDB();
        //    if (TransactionType == 1)
        //    {
        //        user.UserInsert(FirstName, LastName, Email, Password, Company);
        //    }
        //    else if (TransactionType == 2)
        //    {
        //        user.UserUpdate(UserId, CanAddDeleteUser, CanViewRecords, CanAddRecords, CanDeleteRecords, CanEditRecords, CanViewAllRecords);
        //    }
        //    else if (TransactionType == 3)
        //    {
        //        //user.UserDelete(UserId);
        //    }
        //}

        public void Post([FromBody] UserDB userDb)
        {
            UserDB user;
            if (userDb.TransactionType == 1)
            {
                user = new UserDB();
                user.UserInsert(userDb);
            }
            else if (userDb.TransactionType == 2)
            {
                user = new UserDB();
                user.UserUpdate(userDb);
            }
            else if (userDb.TransactionType == 3)
            {
                user = new UserDB();
                user.UserDelete(userDb);
            }
        }
       
    }

}
