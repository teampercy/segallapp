﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SegallBll.DAL;
using SegallBll;

namespace Segall.Controllers.API
{
    public class ReportController : ApiController
    {
        public List<PropertySearchResult> GetGenerateReport(string location, int radius, int loggedInUserId)
        {
            //object[] report= new object[2];
            List<PropertySearchResult> propertySearchResult = new List<PropertySearchResult>();
            try
            {
                ReportDB reportdb = new ReportDB();
                propertySearchResult = reportdb.GenerateReport(location, radius, loggedInUserId);
            }
            catch (Exception ex)
            { }
            return propertySearchResult;
        }

        [HttpPost]
        public List<PropertySearchResult> GenerateReport(PropertyFilterDB propfilters)
        {
            //object[] report= new object[2];
            List<PropertySearchResult> propertySearchResult = new List<PropertySearchResult>();
            try
            {
                //ReportDB reportdb = new ReportDB();
                propertySearchResult = propfilters.GetPropertySearchResults(); 
                                
                //propertySearchResult = reportdb.GenerateReport(location, radius, loggedInUserId);
            }
            catch (Exception ex)
            { }
            return propertySearchResult;
        }

        public IEnumerable<usp_PropertiesSearch_Result> GetGenerateReport(string location, int radius, Int64 loggedInUserId, int usertype)
        {
            object[] report = new object[2];
            IEnumerable<usp_PropertiesSearch_Result>  props = null;
            try
            {
                //ReportDB reportdb = new ReportDB();
                //report = reportdb.GenerateReport(location, radius, loggedInUserId);
                ////props = (usp_PropertiesSearch_Result[])report[0];
                //props = (IEnumerable<usp_PropertiesSearch_Result>)report[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return props;
                       
        }


        public bool GetconvertHtmltoPDf(string htmlstring)
        {
            return true;
        }
    }
}
