﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SegallBll.DAL;

namespace Segall.Controllers.API
{
    public class PropertyFilterController : ApiController
    {
        public object Post([FromBody] PropertyFilterDB Filters)
        {
            object searchresult = new object();
            PropertyFilterDB propertyfilterdb = new PropertyFilterDB();
            searchresult = propertyfilterdb.PropertySearch(Filters);
            return searchresult;
        }

        public object GetSavedSearch(Int64 savedSearchId, Int64 loggedinUserId) 
        {
            object searchresult = new object();
            PropertyFilterDB propertyfilterdb = new PropertyFilterDB();
            //searchresult = propertyfilterdb.PropertySearch(Filters);
            //searchresult = propertyfilterdb.PropertySavedSearch(savedSearchId, loggedinUserId);
            return searchresult;
        }

        //public object[] GetSearchResults(object)
        //{
        //    object[] Property = new object[4];

        //    PropertyBasicDB propertybasicdb = new PropertyBasicDB();
        //    Property = propertybasicdb.PropertyDetailsGet(PropertyInfo);
        //    return Property;
        //}
    }
}
