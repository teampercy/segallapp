﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SegallBll;
using SegallBll.DAL;
using System.Data.Objects;
using System.Data;

namespace Segall.Controllers.API
{
    public class PropertyBasicController : ApiController
    {
        public Int64 Post(PropertyBasic propertybasic)
        {
            Int64 PropertyId;
            PropertyBasicDB propertybasicdb = new PropertyBasicDB();
            if (propertybasic.PropertyId > 0)
            {
                PropertyId = propertybasicdb.PropertyBasicUpdate(propertybasic);
                return PropertyId;
            }
            else
            {
                PropertyId = propertybasicdb.PropertyBasicAdd(propertybasic);
                return PropertyId;
            }
        }        

        public PropertyInfoDetails GetPropertyInfo(Int64 PropertyInfo)
        {
            object[] Property = new object[2];
            PropertyInfoDetails propertyInfodetails = new PropertyInfoDetails();
            PropertyBasicDB propertybasicdb = new PropertyBasicDB();
            propertyInfodetails = propertybasicdb.PropertyDetailsGet(PropertyInfo);
            return propertyInfodetails;
        }


        public List<string> GetPropertyDetailInfo(string TextToSearch)
        {
            PropertyBasicDB propertybasicdb = new PropertyBasicDB();
            List<string> PropertyResults = new List<string>();
            PropertyResults = propertybasicdb.GetPropertyDetailInfo(TextToSearch);
            return PropertyResults;
        }


        public PropertyDetailDB GetPropertyDetails(Int64 PropertyId)
        {
            PropertyDetailDB propertydetails = new PropertyDetailDB();
            PropertyBasicDB propertybasicdb = new PropertyBasicDB();
            propertydetails = propertybasicdb.PropertyDetails(PropertyId);
            return propertydetails;
        }

        public object[] GetPropertyMapPoint(string location, int radius)
        {
            PropertyBasicDB propertybasicdb = new PropertyBasicDB();
            //if (location.Contains("*"))
            //{
            //    string lat = "";
            //    string lng = "";
            //    string zipcode="";
            //    lat = location.Substring(0, location.IndexOf("*"));
            //    lng = location.Substring(Convert.ToInt32(location.IndexOf("*") + 1), location.Length - location.IndexOf("*") - 1);
            //    zipcode=propertybasicdb.GetZipCode(lat, lng);
            //    location = zipcode;
            //}
            object[] MapPoint = new object[1];

            MapPoint = propertybasicdb.PropertyMapPoint(location, radius);
            return MapPoint;
        }

        public object[] GetPropertybasic()
        {
            object[] propertybasicall = new object[1];
            PropertyBasicDB propertybasicdb = new PropertyBasicDB();
            propertybasicall[1] = propertybasicdb.GetPropertybasic();
            return propertybasicall;
        }

        public Int32 UpdateSizeAndVolume(Int32 PropertyId, decimal size)
        {
            PropertyBasicDB propertybasicdb = new PropertyBasicDB();
            return propertybasicdb.UpdateSizeAndVolume(PropertyId, size);           
        }
        //public string Delete(int PropertyId)
        //{
        //    PropertyBasicDB propertybasicdb = new PropertyBasicDB();
        //    propertybasicdb.deletePropertyBasic(PropertyId);
        //    return "deleted";
        //}
       
    }
}
