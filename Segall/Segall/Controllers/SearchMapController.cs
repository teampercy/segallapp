﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using SegallBll.DAL;
using SegallBll.ViewModel;
using SegallBll;

namespace Segall.Controllers
{
    public class SearchMapController : CustomController
    {
        //
        // GET: /SearchMap/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ShowMap(FormCollection form)
        {
            if (form["hdnIsBrowseMap"] != null && form["hdnIsBrowseMap"] != "")
            {
                if (Convert.ToBoolean(form["hdnIsBrowseMap"]) == false)
                {
                    ViewBag.hdnLocation = form["txtSearchLocation"] != "" ? form["txtSearchLocation"] : "";
                    TempData["Location"] = ViewBag.hdnLocation;
                    //added by percy
                    if (form["txtSearchLocation"] != "")
                    {
                        TempData["propertyFilter"] = null;
                    }
                }
                else
                {
                    ViewBag.hdnLocation = "-1";
                    TempData["Location"] = ViewBag.hdnLocation;
                }
                ViewBag.hdnPageInfo = "HomePage";
            }

            else
            {
                ViewBag.hdnLocation = TempData["Location"];
                ViewBag.hdnPageInfo = "SaveSearch";
            }

            return View();
        }

        public ActionResult ShowSearchMap(string Location)
        {
            TempData["Location"] = Location;
            return RedirectToAction("ShowMap");
            //return View();
        }

        public ActionResult GetPropertyInfoById(Int64 propertyid, string Location)
        {
            //propertyid = 12751;
            PropertyInfoViewModel Viewmodel = new PropertyInfoViewModel();
            var client = new HttpClient();
            var UserUrl = Url.RouteUrl(
                "DefaultApi",
                new { httproute = "", controller = "PropertyBasic", PropertyInfo = propertyid },
                Request.Url.Scheme
                );
            var report = client.GetAsync(UserUrl)
                .ContinueWith((userWithResponse) =>
                {

                    var response = userWithResponse.Result;
                    response.EnsureSuccessStatusCode();
                    Viewmodel.propertyInfoDetails = response.Content.ReadAsAsync<PropertyInfoDetails>().Result;
                    userWithResponse.Wait();
                });
            report.Wait();
            SegallEntities sgl = new SegallEntities();
            SegallBll.User LoggedInUser = new SegallBll.User();
            LoggedInUser = (SegallBll.User)Session["LoggedInUser"];
            Viewmodel.propertyaccessright = sgl.usp_PropertyAccessRightsDetailsForUser(LoggedInUser.UserId,propertyid).ToList();


            String HtmlStringPropertyInfo = RenderPartialViewToString("_PropertyInfo", Viewmodel);
            return Json(HtmlStringPropertyInfo);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SaveFilterCriteria(PropertyFilterDB filter)
        {
            TempData["propertyFilter"] = filter;
            Session["propertyFilter"] = filter;
            return Json(1);
            //return RedirectToAction("ShowMap");
        }
        public ActionResult SaveLocationSession(string Location)
        {
            TempData["Location"] = Location;
            Session["Location"] = Location;
            TempData["propertyFilter"]=null;
            return Json(1);
        }

        
        public JsonResult GetMapResults(string location, string radius, string PageInfo)
        {
            PropertyFilterDB propfilter = new PropertyFilterDB();
            SegallEntities sgl = new SegallEntities();
            SegallBll.User LoggedInUser = new SegallBll.User();
            LoggedInUser = (SegallBll.User)Session["LoggedInUser"];
            TempData["Location"] = location;
            if (location != "")
            {
                //Not sure why PageInfo is checked here.
                //if (PageInfo != "HomePage")
                //{
                    //temp commented by percy
                if (TempData["propertyFilter"] != null)
                {
                    propfilter = (PropertyFilterDB)TempData["propertyFilter"];
                    //if (propfilter.IsSavedSearch == false)
                    //{
                    //    propfilter.PropertyName = null;
                    //}
                }
                //}

                propfilter.location = location.Trim();
                if (radius == "") radius = "10";
                propfilter.Distance = Convert.ToInt16(radius);
                TempData["propertyFilter"] = propfilter;
            }
            else
            {
                //Not sure why PageInfo is checked here.
                //if (PageInfo != "HomePage")
                //{
                    if (TempData["propertyFilter"] != null)
                    {
                        propfilter = (PropertyFilterDB)TempData["propertyFilter"];
                        //Session["propertyFilter"] = propfilter;
                    }
                //}
            }
            propfilter.LoggedInUser = LoggedInUser.UserId;
            // The below call should be actually to the API controller.           
            //List<usp_PropertiesSearch_Result> propertySearchResult = new List<usp_PropertiesSearch_Result>();
            var propertySearchResult = propfilter.GetMapResults();

            //var myData = new[] { new { first = "Jan'e", last = "Doe" }, new { first = "John", last = "Doe" } }; 
            //Added by Jaywanti on 08-07-2016
            Session["FilterPolygonArray"] = null;
           // return Json(propertySearchResult, JsonRequestBehavior.AllowGet);
            var jsonResult = Json(propertySearchResult, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public ActionResult AddToFavourite(Int64 propertyid, Int64 UserId)
        {
            string Message = "";
            var client = new HttpClient();

            var UserUrl = Url.RouteUrl(
                "DefaultApi",
                new { httproute = "", controller = "PropertyFavourite", propertyid = propertyid, UserId = UserId, type = "Insert" },
                Request.Url.Scheme
                );
            var report = client.PostAsJsonAsync(UserUrl, "")
                .ContinueWith((userWithResponse) =>
                {
                    var response = userWithResponse.Result;
                    response.EnsureSuccessStatusCode();
                    Message = response.Content.ReadAsAsync<String>().Result;
                    userWithResponse.Wait();
                });
            report.Wait();
            return Json(1);
        }

        public ActionResult DeleteFromFavourite(Int64 propertyid, Int64 UserId)
        {
            string Message = "";
            var client = new HttpClient();

            var UserUrl = Url.RouteUrl(
                "DefaultApi",
                new { httproute = "", controller = "PropertyFavourite", propertyid = propertyid, UserId = UserId, type = "Delete" },
                Request.Url.Scheme
                );
            var report = client.PostAsJsonAsync(UserUrl, "")
                .ContinueWith((userWithResponse) =>
                {
                    var response = userWithResponse.Result;
                    response.EnsureSuccessStatusCode();
                    Message = response.Content.ReadAsAsync<String>().Result;
                    userWithResponse.Wait();
                });
            report.Wait();
            return Json(1);
        }

        public ActionResult GetFavouriteList(Int64 Userid)
        {
            PropertyFavouriteViewModel ViewModel = new PropertyFavouriteViewModel();
            var client = new HttpClient();
            var UserUrl = Url.RouteUrl(
                "DefaultApi",
                new { httproute = "", controller = "PropertyFavourite", UserId = Userid },
                Request.Url.Scheme
                );
            var report = client.GetAsync(UserUrl)
                .ContinueWith((userWithResponse) =>
                {
                    var response = userWithResponse.Result;
                    response.EnsureSuccessStatusCode();
                    ViewModel.PropertyFavouriteList = response.Content.ReadAsAsync<List<usp_GetPropertyFavouriteUserList_Result>>().Result;


                });
            report.Wait();
            return Json(ViewModel.PropertyFavouriteList.ToList());
        }

        public ActionResult ClearFavouriteList()
        {
            string Message = "";
            SegallEntities sgl = new SegallEntities();
            SegallBll.User LoggedInUser = new SegallBll.User();
            LoggedInUser = (SegallBll.User)Session["LoggedInUser"];
            var client = new HttpClient();

            var UserUrl = Url.RouteUrl(
                "DefaultApi",
                new { httproute = "", controller = "PropertyFavourite", propertyid = 0, UserId = LoggedInUser.UserId, type = "DeleteAll" },
                Request.Url.Scheme
                );
            var report = client.PostAsJsonAsync(UserUrl, "")
                .ContinueWith((userWithResponse) =>
                {
                    var response = userWithResponse.Result;
                    response.EnsureSuccessStatusCode();
                    Message = response.Content.ReadAsAsync<String>().Result;
                    userWithResponse.Wait();
                });
            report.Wait();
            return Json("1");
        }
    }
}
