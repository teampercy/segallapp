﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Segall.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5// Type: Segall.Utility.Utility
// Assembly: MVCWebAPITest, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: D:\Projects\CPSVRC\Server_Project\GeoApp\bin\MVCWebAPITest.dll

using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Web;
using Winnovative.WnvHtmlConvert;

namespace Segall.Utility
{
  public class Utility
  {
    public static bool sendEmail(string to, string from, string subject, string body, List<string> attachmentList)
    {
      return Utility.sendEmail(new List<string>()
      {
        to
      }, from, subject, body, attachmentList);
    }

    public static bool sendEmail(List<string> to, string from, string subject, string body, List<string> attachmentList)
    {
      bool flag = false;
      try
      {
        MailMessage message = new MailMessage();
        message.BodyEncoding = Encoding.Default;
        message.IsBodyHtml = true;
        if (to.Count > 0)
        {
          foreach (string addresses in to)
            message.To.Add(addresses);
        }
        message.From = new MailAddress(from, from);
        message.Subject = subject;
        message.Body = body;
        foreach (string str in attachmentList)
        {
          if (File.Exists(str))
          {
            Attachment attachment = new Attachment(str);
            message.Attachments.Add(attachment);
          }
        }
        new SmtpClient()
        {
          Host = ((object) ConfigurationManager.AppSettings["SMTPHost"]).ToString(),
          Port = Convert.ToInt32(((object) ConfigurationManager.AppSettings["SMTPPort"]).ToString()),
          UseDefaultCredentials = true
        }.Send(message);
        flag = true;
      }
      catch (Exception ex)
      {
      }
      return flag;
    }

    public static byte[] ConvertHTMLStringToPDF(string htmlData)
    {
      try
      {
        string htmlString = htmlData;
        PdfConverter pdfConverter = new PdfConverter();
        pdfConverter.LicenseKey = "OgmRqJy4kkQWgihnkILiGC3ofMK83tlFSiAVpzjJv9Fa6+ZMh+8fNIirK6M+dxWT";
        pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
        pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Best;
        pdfConverter.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
        pdfConverter.PdfDocumentOptions.ShowHeader = true;
        pdfConverter.PdfDocumentOptions.ShowFooter = true;
        pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
        pdfConverter.PdfDocumentOptions.EmbedFonts = true;
        pdfConverter.PdfDocumentOptions.LiveUrlsEnabled = true;
        pdfConverter.RightToLeftEnabled = false;
        pdfConverter.PdfDocumentOptions.LeftMargin = 5;
        pdfConverter.PdfDocumentOptions.RightMargin = 5;
        pdfConverter.PdfDocumentOptions.TopMargin = 0;
        pdfConverter.PdfDocumentOptions.BottomMargin = 0;
        pdfConverter.PdfHeaderOptions.HeaderText = "";
        pdfConverter.PdfFooterOptions.FooterText = "";
        pdfConverter.PdfHeaderOptions.HeaderHeight = 50f;
        pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
        pdfConverter.PdfHeaderOptions.HeaderTextYLocation = 1f;
        pdfConverter.PdfFooterOptions.FooterHeight = 25f;
        pdfConverter.PdfFooterOptions.PageNumberYLocation = 1f;
        pdfConverter.PdfFooterOptions.ShowPageNumber = true;
        pdfConverter.PdfSecurityOptions.CanPrint = true;
        pdfConverter.PdfSecurityOptions.CanEditContent = true;
        pdfConverter.PdfSecurityOptions.UserPassword = "";
        pdfConverter.PdfDocumentInfo.AuthorName = "Segall Groups";
        return pdfConverter.GetPdfBytesFromHtmlString(htmlString);
      }
      catch (Exception ex)
      {
        throw new HttpException(404, "Couldn't generate pdf " + ex.Message);
      }
    }

    public static byte[] ConvertHTMLStringToPDFwithImages(string htmlData, string thisPageURL)
    {
      return new PdfConverter()
      {
        LicenseKey = "OgmRqJy4kkQWgihnkILiGC3ofMK83tlFSiAVpzjJv9Fa6+ZMh+8fNIirK6M+dxWT",
        PdfDocumentOptions = {
          PdfPageSize = PdfPageSize.A4,
          PdfCompressionLevel = PdfCompressionLevel.Normal,
          PdfPageOrientation = PDFPageOrientation.Portrait,
          ShowHeader = false,
          ShowFooter = false,
          GenerateSelectablePdf = true,
          EmbedFonts = true,
          LiveUrlsEnabled = true
        },
        RightToLeftEnabled = false,
        PdfSecurityOptions = {
          CanPrint = true,
          CanEditContent = true,
          UserPassword = ""
        },
        PdfDocumentInfo = {
          AuthorName = "Segall Groups"
        }
      }.GetPdfBytesFromHtmlString(htmlData, thisPageURL);
    }

    public static bool StoreBytesAsFile(byte[] data, string pathWithFileName)
    {
      bool flag = true;
      try
      {
        File.WriteAllBytes(pathWithFileName, data);
      }
      catch (Exception ex)
      {
        flag = false;
      }
      return flag;
    }
  }
}

        public void Delete(int id)
        {
        }
    }
}