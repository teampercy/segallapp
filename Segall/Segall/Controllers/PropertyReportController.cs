﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;

using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using SegallBll.DAL;
using SegallBll.ViewModel;
using CrystalDecisions.CrystalReports.Engine;
using SegallBll;


namespace Segall.Controllers
{
    public class PropertyReportController : CustomController
    {
        //
        // GET: /Report/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ShowReportOLD(string Location)
        {
            ReportViewModel ViewModel = new ReportViewModel();
            if (TempData["Location"] != null)
            {
                Location = TempData["Location"].ToString();
            }
            if (TempData["propertyFilter"] != null)
            {
                ViewModel.propertyFilter = (PropertyFilterDB)TempData["propertyFilter"];

                if (ViewModel.propertyFilter.PropertyName == null)
                {
                    ViewModel.propertyFilter.PropertyName = "Any";
                }
                if (ViewModel.propertyFilter.City == null)
                {
                    ViewModel.propertyFilter.City = "Any";
                }
                if (ViewModel.propertyFilter.State == null)
                {
                    ViewModel.propertyFilter.State = "Any";
                }
                if (ViewModel.propertyFilter.County == null)
                {
                    ViewModel.propertyFilter.County = "Any";
                }
                if (ViewModel.propertyFilter.Zipcode == null)
                {
                    ViewModel.propertyFilter.Zipcode = "Any";
                }
                if (ViewModel.propertyFilter.Category == null)
                {
                    ViewModel.propertyFilter.Category = 1;
                }
                if (ViewModel.propertyFilter.SalesAnuallyFrom == null && ViewModel.propertyFilter.SalesAnuallyTo == null)
                {
                    if (ViewModel.propertyFilter.SalesMonthlyFrom == null && ViewModel.propertyFilter.SalesMonthlyTo == null)
                    {
                        if (ViewModel.propertyFilter.SalesWeeklyFrom == null && ViewModel.propertyFilter.SalesWeeklyTo == null)
                        {
                            ViewModel.sales = "Any";
                            ViewModel.period = "Any";
                        }
                        else
                        {
                            ViewModel.sales = "Weekly";
                            ViewModel.period = ViewModel.propertyFilter.SalesWeeklyFrom.ToString() + " - " + ViewModel.propertyFilter.SalesWeeklyTo.ToString();
                        }
                    }
                    else
                    {
                        ViewModel.sales = "Monthly";
                        ViewModel.period = ViewModel.propertyFilter.SalesMonthlyFrom.ToString() + " - " + ViewModel.propertyFilter.SalesMonthlyTo.ToString();
                    }
                }
                else
                {
                    ViewModel.sales = "Annually";
                    ViewModel.period = ViewModel.propertyFilter.SalesAnuallyFrom.ToString() + " - " + ViewModel.propertyFilter.SalesAnuallyTo.ToString();
                }

                if (ViewModel.GlA == null)
                {
                    ViewModel.GlA = "Any";
                }
                if (ViewModel.Rent == null)
                {
                    ViewModel.Rent = "Any";
                }
                if (ViewModel.Net == null)
                {
                    ViewModel.Net = "Any";
                }
                if (ViewModel.TI == null)
                {
                    ViewModel.TI = "Any";
                }
                if (ViewModel.FreeRent == null)
                {
                    ViewModel.FreeRent = "Any";
                }
                if (ViewModel.Term == null)
                {
                    ViewModel.Term = "Any";
                }
            }
            else
            {
                ViewModel.propertyFilter.PropertyName = "Any";
                ViewModel.propertyFilter.City = "Any";
                ViewModel.propertyFilter.State = "Any";
                ViewModel.propertyFilter.County = "Any";
                ViewModel.propertyFilter.Zipcode = "Any";
                ViewModel.sales = "Any";
                ViewModel.period = "Any";
                ViewModel.GlA = "Any";
                ViewModel.Rent = "Any";
                ViewModel.Net = "Any";
                ViewModel.TI = "Any";
                ViewModel.FreeRent = "Any";
                ViewModel.Term = "Any";
            }
            ViewBag.hdnLocation = Location;
            SegallBll.User user = new SegallBll.User();
            user = (SegallBll.User)Session["LoggedInUser"];

            var client = new HttpClient();

            var UserUrl = Url.RouteUrl(
                "DefaultApi",
                new { httproute = "", controller = "Report", location = Location, radius = 10, loggedInUserId = user.UserId },
                Request.Url.Scheme
                );

            var report = client.GetAsync(UserUrl)
                .ContinueWith((userWithResponse) =>
                {

                    var response = userWithResponse.Result;
                    response.EnsureSuccessStatusCode();
                    ViewModel.propertySearch = response.Content.ReadAsAsync<List<PropertySearchResult>>().Result;
                    userWithResponse.Wait();
                });
            report.Wait();


            TempData["Location"] = Location;
            //using (HttpClient httpClient = new HttpClient())
            //{
            //    Task<String> response = httpClient.GetStringAsync(UserUrl);
            //    return JsonConvert.DeserializeObjectAsync<List<PropertySearchResult>>(response.Result).Result;
            //}

            return View(ViewModel);
        }


        public ActionResult ShowReport(string Location, string PropertyIdtoSkip = "", int PageNo = 1, bool favouriteOnly = false, int VolumeLeaseData = 3)
        {
            SegallBll.User LoggedInUser = new SegallBll.User();
            LoggedInUser = (SegallBll.User)Session["LoggedInUser"];
            ViewBag.hdnPropertyToskip = PropertyIdtoSkip;
            ReportViewModel ViewModel = new ReportViewModel();
            ViewModel.VolumeOrLease = VolumeLeaseData;
            TempData["VolumeOrLease"] = ViewModel.VolumeOrLease;
            //if (TempData["VolumeOrLease"] != null)
            //{
            //    ViewModel.VolumeOrLease = Convert.ToInt32(TempData["VolumeOrLease"].ToString());
            //    TempData["VolumeOrLease"] = ViewModel.VolumeOrLease;
            //}
            //else
            //{
            //    ViewModel.VolumeOrLease = 2;
            //    TempData["VolumeOrLease"] = "2";
            //}
            #region storing tempdata and ViewModel
            //if (Session["FavoutiteOnly"] == null || favouriteOnly == true)
            //{
            //    Session["FavoutiteOnly"] = favouriteOnly;
            //}
            //else
            //{
            //    favouriteOnly = Convert.ToBoolean(Session["FavoutiteOnly"]);
            //}

            //if (Session["propertyFilter"] != null)
            if (Session["propertyFilter"] != null && TempData["propertyFilter"] == null)
            {
                TempData["propertyFilter"] = Session["propertyFilter"];
            }
            if (Session["Location"] != null)
            {
                TempData["Location"] = Session["Location"];
            }

            if (TempData["Location"] != null)
            {
                Location = TempData["Location"].ToString();
                Session["Location"] = TempData["Location"];
            }
            if (TempData["propertyFilter"] != null)
            {
                ViewModel.propertyFilter = (PropertyFilterDB)TempData["propertyFilter"];
                if (favouriteOnly == false && Location != "")
                {
                    ViewModel.propertyFilter.location = Location;
                }
                ViewModel.propertyFilter.LoggedInUser = LoggedInUser.UserId;
                ViewModel.propertyFilter.FavoriteOnly = favouriteOnly;
                ViewModel.propertyFilter.RecPerPage = 10;
                ViewModel.propertyFilter.PageNo = PageNo;
            }
            ViewBag.hdnLocation = Location;

            #endregion

            #region calling Async report

            var client = new HttpClient();
            var UserUrl = Url.RouteUrl(
                "DefaultApi",
                new { httproute = "", controller = "Report" },
                Request.Url.Scheme
                );

            var report = client.PostAsJsonAsync(UserUrl, ViewModel.propertyFilter)
                .ContinueWith((postTask) =>
                    {
                        var response = postTask.Result;
                        response.EnsureSuccessStatusCode();
                        ViewModel.propertySearch = response.Content.ReadAsAsync<List<PropertySearchResult>>().Result;
                        postTask.Wait();
                    });
            report.Wait();
            #endregion

            #region Skip propertyInfo which are Unseleceted

            for (int i = 1; i < PropertyIdtoSkip.Length; )
            {
                string id = PropertyIdtoSkip.Substring(0, PropertyIdtoSkip.IndexOf(","));
                PropertyIdtoSkip = PropertyIdtoSkip.Substring(PropertyIdtoSkip.IndexOf(",") + 1, (PropertyIdtoSkip.Length - PropertyIdtoSkip.IndexOf(",") - 1));
                ViewModel.PropertyToSkip.Add(Convert.ToInt32(id));
            }
            #endregion


            ViewModel.CurrentPage = PageNo;
            TempData["Location"] = Location;
            //TempData["Location"] = ViewModel.propertyFilter.location;
            //Session["Location"] = TempData["Location"];

            TempData["propertyFilter"] = ViewModel.propertyFilter;
            //Added by Jaywanti on 08-07-2016
            if (Session["FilterPolygonArray"] != null)
            {
                string FilterPolygonArray = Session["FilterPolygonArray"].ToString();
                string[] ArrayList = FilterPolygonArray.Split(',');
                List<PropertySearchResult> NewPropertySearchList=new List<PropertySearchResult>();
                for (int j = 0; j < ArrayList.Length; j++)
                {
                    for (int i = 0; i < ViewModel.propertySearch.Count; i++)
                    {
                        if (ViewModel.propertySearch[i].propertySearch.PropertyId == Convert.ToInt64(ArrayList[j]))
                        {
                            NewPropertySearchList.Add(ViewModel.propertySearch[i]);
                        }
                    }
                }
                ViewModel.propertySearch = null;
                ViewModel.propertySearch = NewPropertySearchList;
            }
            return View(ViewModel);
        }

        public ActionResult ShowFavReport(int PageNo = 1)
        {
            #region variables


            //ViewBag.hdnLocation = Location;
            #endregion
            //ViewBag.hdnLocation = Location;
            //return View();
            if (Session["propertyFilter"] != null)
            {
                TempData["propertyFilter"] = Session["propertyFilter"];
            }

            SegallBll.User LoggedInUser = new SegallBll.User();
            LoggedInUser = (SegallBll.User)Session["LoggedInUser"];

            ReportViewModel ViewModel = new ReportViewModel();
            if (TempData["propertyFilter"] != null)
            {
                ViewModel.propertyFilter = (PropertyFilterDB)TempData["propertyFilter"];
                ViewModel.propertyFilter.LoggedInUser = LoggedInUser.UserId;
                ViewModel.propertyFilter.FavoriteOnly = true;
                ViewModel.propertyFilter.RecPerPage = 10;
                ViewModel.propertyFilter.PageNo = PageNo;
            }
            else
            {
                ViewModel.propertyFilter.PropertyName = "";
                ViewModel.propertyFilter.City = "";
                ViewModel.propertyFilter.State = "";
                ViewModel.propertyFilter.County = "";
                ViewModel.propertyFilter.Zipcode = "";
                ViewModel.sales = "";
                ViewModel.period = "";
                ViewModel.GlA = "";
                ViewModel.Rent = "";
                ViewModel.Net = "";
                ViewModel.TI = "";
                ViewModel.FreeRent = "";
                ViewModel.Term = "";
                ViewModel.propertyFilter.FavoriteOnly = true;
            }
            //ViewBag.hdnLocation = Location;

            TempData["propertyFilter"] = ViewModel.propertyFilter;

            var client = new HttpClient();
            var UserUrl = Url.RouteUrl(
                "DefaultApi",
                new { httproute = "", controller = "Report" },
                Request.Url.Scheme
                );

            var report = client.PostAsJsonAsync(UserUrl, ViewModel.propertyFilter)
                .ContinueWith((postTask) =>
                {
                    var response = postTask.Result;
                    response.EnsureSuccessStatusCode();
                    ViewModel.propertySearch = response.Content.ReadAsAsync<List<PropertySearchResult>>().Result;
                    postTask.Wait();
                });
            report.Wait();

            #region Generate Pdf of Report
            //HtmlString = RenderViewToString("ShowReport", ViewModel);
            //HtmlString = HtmlString.Substring((HtmlString.IndexOf("GeneratePdf") + 27), (HtmlString.Length - HtmlString.IndexOf("GeneratePdf") - 27));
            //HtmlString = "<table width=100%><tbody>" + HtmlString;

            //filename = "Chipotle Pikesville Search.pdf";
            //baseFolder = ConfigurationManager.AppSettings["AttachmentPath"].ToString();
            //if (!Directory.Exists(Server.MapPath(baseFolder)))
            //{
            //    Directory.CreateDirectory(Server.MapPath(baseFolder));
            //}
            //filepath = Path.Combine(Server.MapPath(baseFolder + filename));
            //pdfBytes = Utility.Utility.ConvertHTMLStringToPDF(HtmlString);
            //Utility.Utility.StoreBytesAsFile(pdfBytes, filepath);
            #endregion
            ViewModel.CurrentPage = PageNo;
            TempData["Location"] = "";

            return View("ShowReport", ViewModel);
        }

        public ActionResult deletereport()
        {
            TempData["propertyFilter"] = null;
            Session["propertyFilter"] = null;
            TempData["Location"] = null;
            Session["Location"] = null;
            return Json("1");
        }

        public FileResult Download(string Reportname, string PropertyIdtoSkip = "")
        {
            #region variables

            string HtmlString = "";
            string filename = ViewBag.hdnReportNameNew;
            string baseFolder = "";
            string filepath = "";
            byte[] pdfBytes = null;

            #endregion


            SegallBll.User user = new SegallBll.User();
            user = (SegallBll.User)Session["LoggedInUser"];
            ReportViewModel ViewModel = new ReportViewModel();
            try
            {
                if (TempData["VolumeOrLease"] != null)
                {
                    ViewModel.VolumeOrLease = Convert.ToInt32(TempData["VolumeOrLease"].ToString());
                    TempData["VolumeOrLease"] = ViewModel.VolumeOrLease;
                }
                else
                {
                    ViewModel.VolumeOrLease = 2;
                    TempData["VolumeOrLease"] = "2";
                }
                if (Session["propertyFilter"] != null)
                {
                    TempData["propertyFilter"] = Session["propertyFilter"];
                }
                else
                { Session["propertyFilter"] = TempData["propertyFilter"]; }
                if (TempData["propertyFilter"] != null)
                {
                    ViewModel.propertyFilter = (PropertyFilterDB)TempData["propertyFilter"];
                    ViewModel.propertyFilter.LoggedInUser = user.UserId;

                }
                if (Session["Location"] != null)
                {
                    TempData["Location"] = Session["Location"];
                }
                //if (TempData["Location"] != null)
                //{

                //    ViewModel.propertyFilter.location = TempData["Location"].ToString();
                //    ViewModel.propertyFilter.LoggedInUser = user.UserId;
                //}

                #region paging parametrs
                ViewModel.propertyFilter.PageNo = 1;
                ViewModel.propertyFilter.RecPerPage = 100;
                ViewModel.CurrentPage = 1;
                if (TempData["Location"] != null)
                {
                    string Location = TempData["Location"].ToString();
                    if (Location != "")
                    {
                        ViewModel.propertyFilter.location = Location;
                    }
                }
                #endregion

                var client = new HttpClient();
                var UserUrl = Url.RouteUrl(
                    "DefaultApi",
                    new { httproute = "", controller = "Report" },
                    Request.Url.Scheme
                    );

                var report = client.PostAsJsonAsync(UserUrl, ViewModel.propertyFilter)
                    .ContinueWith((postTask) =>
                    {
                        var response = postTask.Result;
                        response.EnsureSuccessStatusCode();
                        ViewModel.propertySearch = response.Content.ReadAsAsync<List<PropertySearchResult>>().Result;
                        postTask.Wait();
                    });
                report.Wait();
                #region Skip propertyInfo which are Unseleceted

                for (int i = 1; i < PropertyIdtoSkip.Length; )
                {
                    string id = PropertyIdtoSkip.Substring(0, PropertyIdtoSkip.IndexOf(","));
                    PropertyIdtoSkip = PropertyIdtoSkip.Substring(PropertyIdtoSkip.IndexOf(",") + 1, (PropertyIdtoSkip.Length - PropertyIdtoSkip.IndexOf(",") - 1));
                    ViewModel.PropertyToSkip.Add(Convert.ToInt32(id));
                }
                #endregion

                if (Session["FilterPolygonArray"] != null)
                {
                    string FilterPolygonArray = Session["FilterPolygonArray"].ToString();
                    string[] ArrayList = FilterPolygonArray.Split(',');
                    List<PropertySearchResult> NewPropertySearchList = new List<PropertySearchResult>();
                    for (int j = 0; j < ArrayList.Length; j++)
                    {
                        for (int i = 0; i < ViewModel.propertySearch.Count; i++)
                        {
                            if (ViewModel.propertySearch[i].propertySearch.PropertyId == Convert.ToInt64(ArrayList[j]))
                            {
                                NewPropertySearchList.Add(ViewModel.propertySearch[i]);
                            }
                        }
                    }
                    ViewModel.propertySearch = null;
                    ViewModel.propertySearch = NewPropertySearchList;
                }
                HtmlString = RenderViewToString("ShowReport", ViewModel);
                HtmlString = HtmlString.Substring((HtmlString.IndexOf("GeneratePdf") + 27), (HtmlString.Length - HtmlString.IndexOf("GeneratePdf") - 27));
                HtmlString = "<table width=100%><tbody>" + HtmlString;

                #region downloading the report

                #region skip the pager

                HtmlString = HtmlString.Substring(0, HtmlString.IndexOf("pagerstart") - 10);
                #endregion
                filename = "SearchReport_" + DateTime.Now.ToString("MMddyyyyhhmmss") + ".pdf";
                baseFolder = ConfigurationManager.AppSettings["AttachmentPath"].ToString();
                if (!Directory.Exists(Server.MapPath(baseFolder)))
                {
                    Directory.CreateDirectory(Server.MapPath(baseFolder));
                }
                filepath = Path.Combine(Server.MapPath(baseFolder + filename));
                string thisPageURL = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;
                pdfBytes = Utility.Utility.ConvertHTMLStringToPDFwithImages(HtmlString, thisPageURL);
                Utility.Utility.StoreBytesAsFile(pdfBytes, filepath);

                //var fs =  System.IO.File.OpenRead(Server.MapPath("/some/path/" + fileName));
                return File(filepath, "application/zip", filename);
            }
            catch (Exception Ex)
            {
                throw new HttpException(404, "Couldn't find " + filename);
            }
                #endregion

        }

        public ActionResult ConvertReporttoPDf(string Location, string PropertyIdtoSkip = "")
        {

            #region variables

            string HtmlString = "";
            string filename = "";
            string baseFolder = "";
            string filepath = "";
            byte[] pdfBytes = null;

            #endregion


            SegallBll.User user = new SegallBll.User();
            user = (SegallBll.User)Session["LoggedInUser"];
            ReportViewModel ViewModel = new ReportViewModel();
            try
            {
                if (Session["propertyFilter"] != null)
                {
                    TempData["propertyFilter"] = Session["propertyFilter"];
                }
                else
                { Session["propertyFilter"] = TempData["propertyFilter"]; }
                if (TempData["propertyFilter"] != null)
                {
                    ViewModel.propertyFilter = (PropertyFilterDB)TempData["propertyFilter"];
                    ViewModel.propertyFilter.LoggedInUser = user.UserId;

                }
                if (Session["Location"] != null)
                {
                    TempData["Location"] = Session["Location"];
                }
                if (TempData["Location"] != null)
                {
                    if (TempData["Location"].ToString() != "")
                    {
                        ViewModel.propertyFilter.location = TempData["Location"].ToString();
                    }

                }

                #region paging parametrs
                ViewModel.propertyFilter.PageNo = 1;
                ViewModel.propertyFilter.RecPerPage = 100;
                ViewModel.CurrentPage = 1;
                #endregion

                var client = new HttpClient();
                var UserUrl = Url.RouteUrl(
                    "DefaultApi",
                    new { httproute = "", controller = "Report" },
                    Request.Url.Scheme
                    );

                var report = client.PostAsJsonAsync(UserUrl, ViewModel.propertyFilter)
                    .ContinueWith((postTask) =>
                    {
                        var response = postTask.Result;
                        response.EnsureSuccessStatusCode();
                        ViewModel.propertySearch = response.Content.ReadAsAsync<List<PropertySearchResult>>().Result;
                        postTask.Wait();
                    });
                report.Wait();


                #region Skip propertyInfo which are Unseleceted

                for (int i = 1; i < PropertyIdtoSkip.Length; )
                {
                    string id = PropertyIdtoSkip.Substring(0, PropertyIdtoSkip.IndexOf(","));
                    PropertyIdtoSkip = PropertyIdtoSkip.Substring(PropertyIdtoSkip.IndexOf(",") + 1, (PropertyIdtoSkip.Length - PropertyIdtoSkip.IndexOf(",") - 1));
                    ViewModel.PropertyToSkip.Add(Convert.ToInt32(id));
                }
                #endregion
                //Added by Jaywanti on 08-07-2016
                if (Session["FilterPolygonArray"] != null)
                {
                    string FilterPolygonArray = Session["FilterPolygonArray"].ToString();
                    string[] ArrayList = FilterPolygonArray.Split(',');
                    List<PropertySearchResult> NewPropertySearchList = new List<PropertySearchResult>();
                    for (int j = 0; j < ArrayList.Length; j++)
                    {
                        for (int i = 0; i < ViewModel.propertySearch.Count; i++)
                        {
                            if (ViewModel.propertySearch[i].propertySearch.PropertyId == Convert.ToInt64(ArrayList[j]))
                            {
                                NewPropertySearchList.Add(ViewModel.propertySearch[i]);
                            }
                        }
                    }
                    ViewModel.propertySearch = null;
                    ViewModel.propertySearch = NewPropertySearchList;
                }
                HtmlString = RenderViewToString("ShowReport", ViewModel);
                HtmlString = HtmlString.Substring((HtmlString.IndexOf("GeneratePdf") + 27), (HtmlString.Length - HtmlString.IndexOf("GeneratePdf") - 27));
                HtmlString = "<table width=100%><tbody>" + HtmlString;

                #region downloading the report

                #region skip the pager

                HtmlString = HtmlString.Substring(0, HtmlString.IndexOf("pagerstart") - 10);
                #endregion


                //filename = "Chipotle Pikesville Search.pdf";
                //filename = "SearchReport_" + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Year.ToString() + ".pdf";
                //filename = "SearchReport_" + DateTime.Now.Ticks.ToString() + ".pdf";
                filename = "SearchReport_" + DateTime.Now.ToString("MMddyyyyhhmmss") + ".pdf";
                baseFolder = ConfigurationManager.AppSettings["AttachmentPath"].ToString();

                if (!Directory.Exists(Server.MapPath(baseFolder)))
                {
                    //Directory.Delete(Server.MapPath(baseFolder), true);
                    Directory.CreateDirectory(Server.MapPath(baseFolder));
                }
                //Directory.CreateDirectory(Server.MapPath(baseFolder));
                filepath = Path.Combine(Server.MapPath(baseFolder + filename));
                string thisPageURL = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;
                pdfBytes = Utility.Utility.ConvertHTMLStringToPDFwithImages(HtmlString, thisPageURL);
                Utility.Utility.StoreBytesAsFile(pdfBytes, filepath);

                #endregion
                return Json(filepath);
            }
            catch (Exception ex)
            {
                throw new HttpException(404, "Couldn't find " + filename);
            }

        }

        //[ValidateInput(false)]
        public ActionResult HtmltoPdf(FormCollection form)
        {
            string filepath = "";
            string ReportName = "";
            if (form != null)
            {
                filepath = form["hdnReportPath"];
                ReportName = form["hdnReportName"];
            }
            return RedirectToAction("SendMail", "Email", new { attachmentPath = filepath, ReportName = ReportName });

            //return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdatePropertyFilter()
        {
            ReportViewModel ViewModel = new ReportViewModel();
            ViewModel.propertyFilter = (PropertyFilterDB)TempData["propertyFilter"];
            if (ViewModel.propertyFilter != null)
            {
                ViewModel.propertyFilter.FavoriteOnly = false;
                ViewModel.propertyFilter.PageNo = 1;
                ViewModel.propertyFilter.RecPerPage = 1000;
                //TempData["propertyFilter"] = ViewModel.propertyFilter;
                Session["FavoutiteOnly"] = null;
            }
            return Json("1", JsonRequestBehavior.AllowGet);
        }

        public FileResult Report()
        {
            ReportClass rptH = new ReportClass();
            SegallBll.User LoggedInUser = new SegallBll.User();
            LoggedInUser = (SegallBll.User)Session["LoggedInUser"];

            ReportViewModel ViewModel = new ReportViewModel();

            #region storing tempdata and ViewModel
            if (Session["FavoutiteOnly"] != null)
            {
                ViewModel.propertyFilter.FavoriteOnly = Convert.ToBoolean(Session["FavoutiteOnly"]);
            }
            else
            {
                ViewModel.propertyFilter.FavoriteOnly = false;
            }

            //if (Session["propertyFilter"] != null)
            //if (Session["propertyFilter"] != null && TempData["propertyFilter"] == null)
            //{
            //    TempData["propertyFilter"] = Session["propertyFilter"];
            //}
            //if (Session["Location"] != null)
            //{
            //    TempData["Location"] = Session["Location"];
            //}

            //if (TempData["Location"] != null)
            //{
            //    Location = TempData["Location"].ToString();
            //}
            if (TempData["propertyFilter"] != null)
            {
                ViewModel.propertyFilter = (PropertyFilterDB)TempData["propertyFilter"];
                ViewModel.propertyFilter.LoggedInUser = LoggedInUser.UserId;
                ViewModel.propertyFilter.RecPerPage = 100000;
                ViewModel.propertyFilter.PageNo = 1;
                ViewModel.propertyFilter.location = Session["Location"].ToString();
            }


            #endregion


            CrystalDecisions.Shared.TableLogOnInfo myLogin = default(CrystalDecisions.Shared.TableLogOnInfo);

            rptH.FileName = Server.MapPath("~/Report/Crystal Report/PropertySearch.rpt");
            rptH.Load();
            rptH.Refresh();

            foreach (Table myTable in rptH.Database.Tables)
            {
                myLogin = myTable.LogOnInfo;

                var _with1 = myLogin;
                _with1.ConnectionInfo.ServerName = ConfigurationManager.AppSettings["ServerName"].ToString();
                _with1.ConnectionInfo.UserID = ConfigurationManager.AppSettings["Username"].ToString();
                _with1.ConnectionInfo.Password = ConfigurationManager.AppSettings["Password"].ToString();
                _with1.ConnectionInfo.DatabaseName = ConfigurationManager.AppSettings["DatabaseName"].ToString();

                myTable.ApplyLogOnInfo(myLogin);

                if (myTable.TestConnectivity())
                {
                    if ((myTable.Location.IndexOf(".") > 0))
                    {
                        myTable.Location = myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1);
                    }
                    else
                    {
                        myTable.Location = myTable.Location;
                    }
                }
            }

            rptH.SetParameterValue("@LoggedInUserId", LoggedInUser.UserId);
            rptH.SetParameterValue("@location", ViewModel.propertyFilter.location);
            rptH.SetParameterValue("@Distance", ViewModel.propertyFilter.Distance);
            rptH.SetParameterValue("@Page", ViewModel.propertyFilter.PageNo);
            rptH.SetParameterValue("@RecsPerPage", ViewModel.propertyFilter.RecPerPage);
            rptH.SetParameterValue("@FavoriteOnly", ViewModel.propertyFilter.FavoriteOnly);
            rptH.SetParameterValue("@PropertyName", ViewModel.propertyFilter.PropertyName);
            rptH.SetParameterValue("@City", ViewModel.propertyFilter.City);
            rptH.SetParameterValue("@County", ViewModel.propertyFilter.County);
            rptH.SetParameterValue("@State", ViewModel.propertyFilter.State);
            rptH.SetParameterValue("@Zipcode", ViewModel.propertyFilter.Zipcode);
            rptH.SetParameterValue("@SizeFrom", ViewModel.propertyFilter.SizeFrom);
            rptH.SetParameterValue("@SizeTo", ViewModel.propertyFilter.SizeTo);
            rptH.SetParameterValue("@Category", ViewModel.propertyFilter.Category);
            rptH.SetParameterValue("@SalesWeeklyFrom", ViewModel.propertyFilter.SalesWeeklyFrom);
            rptH.SetParameterValue("@SalesWeeklyTo", ViewModel.propertyFilter.SalesWeeklyTo);
            rptH.SetParameterValue("@SalesMonthlyFrom", ViewModel.propertyFilter.SalesMonthlyFrom);
            rptH.SetParameterValue("@SalesMonthlyTo", ViewModel.propertyFilter.SalesMonthlyTo);
            rptH.SetParameterValue("@SalesAnuallyFrom", ViewModel.propertyFilter.SalesAnuallyFrom);
            rptH.SetParameterValue("@SalesAnuallyTo", ViewModel.propertyFilter.SalesAnuallyTo);
            rptH.SetParameterValue("@SalesPeriodFrom", ViewModel.propertyFilter.SalesPeriodFrom);
            rptH.SetParameterValue("@SalesPeriodTo", ViewModel.propertyFilter.SalesPeriodTo);
            rptH.SetParameterValue("@DealType", ViewModel.propertyFilter.DealType);
            rptH.SetParameterValue("@DealSubType", ViewModel.propertyFilter.DealSubType);
            rptH.SetParameterValue("@LandSizeFrom", ViewModel.propertyFilter.LandSizeFrom);
            rptH.SetParameterValue("@LandSizeTo", ViewModel.propertyFilter.LandSizeTo);
            rptH.SetParameterValue("@BuildingSizeFrom", ViewModel.propertyFilter.BuildingSizeFrom);
            rptH.SetParameterValue("@BuildingSizeTo", ViewModel.propertyFilter.BuildingSizeTo);
            rptH.SetParameterValue("@RentSizeFrom", ViewModel.propertyFilter.RentSizeFrom);
            rptH.SetParameterValue("@RentSizeTo", ViewModel.propertyFilter.RentSizeTo);
            rptH.SetParameterValue("@RentAmountFrom", ViewModel.propertyFilter.RentAmountFrom);
            rptH.SetParameterValue("@RentAmountTo", ViewModel.propertyFilter.RentAmountTo);

            Stream stream = rptH.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);

            rptH.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"C:\Users\Sumit Iyer\SearchReport_" + DateTime.Now.ToString("MMddyyyyhhmmss") + ".pdf");
            TempData["propertyFilter"] = ViewModel.propertyFilter;
            return File(stream, "application/pdf", "SearchReport_" + DateTime.Now.ToString("MMddyyyyhhmmss") + ".pdf");
        }
        public JsonResult SetPloygonShapeData(string FilterPolygon)
        {
            //Added by Jaywanti on 08-07-2016
            if (FilterPolygon.Length > 0)
            {
                FilterPolygon = FilterPolygon.Substring(0, FilterPolygon.Length - 1);
            }
            Session["FilterPolygonArray"] = FilterPolygon;
            return Json("");
        }
        public JsonResult DeletePolygonShapeList()
        {
            //Added by Jaywanti on 08-07-2016
            Session["FilterPolygonArray"] = null;
            return Json("");
        }

    }
}
