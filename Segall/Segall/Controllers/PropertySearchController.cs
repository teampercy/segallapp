﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Objects;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using SegallBll.Custom_Class;
using SegallBll.DAL;
using SegallBll; 


namespace Segall.Controllers
{
    public class PropertySearchController : Controller
    {
        //
        // GET: /PropertySearch/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ShowSavedEntry()
        {
            Response.Write(TempData["Message"]);
            return View();
        }

        public ActionResult DeleteSAvedSearch(HttpContent form)
        {
            string Message = "";

            string str = ViewBag.hdnSavedSearchDelete;
            var client = new HttpClient();
            var UserUrl = Url.RouteUrl(
                "DefaultApi",
                new { httproute = "", controller = "SavedSearch", id = 5 },
                Request.Url.Scheme
                );
            var user = client.PostAsync(UserUrl, form)
                .ContinueWith((userWithResponse) =>
                {
                    var response = userWithResponse.Result;
                    response.EnsureSuccessStatusCode();
                    Message = response.Content.ReadAsAsync<String>().Result;
                    userWithResponse.Wait();
                });
            user.Wait();
            TempData["Message"] = Message;
            return RedirectToAction("ShowSavedEntry");
        }

        public ActionResult saveSavedSearch(string savedsearchName, string Location)
        {
            PropertySavedSearchDB savedSearchobj = new PropertySavedSearchDB();
            savedSearchobj.type = "post";
            savedSearchobj.savesearch.SavedSearchName = savedsearchName;
            savedSearchobj.savesearch.SearchSavedDate = DateTime.Now;
            if (TempData["Location"] != null)
            {
                Location = (string)TempData["Location"];
                savedSearchobj.savesearch.Location = Location;
                //savedSearchobj.savesearch.Radius = 10;
                
            }
            if (TempData["propertyFilter"] != null)
            {
                var propertyFilter = (PropertyFilterDB)TempData["propertyFilter"];
                
                savedSearchobj.savesearch.BuildingSizeLower = propertyFilter.BuildingSizeTo;
                savedSearchobj.savesearch.BuildingSizeUpper = propertyFilter.BuildingSizeFrom;
                savedSearchobj.savesearch.category=propertyFilter.Category;
                savedSearchobj.savesearch.City = propertyFilter.City;
                //savedSearchobj.savesearch.ConsiderationAmtLower = 0;
                //savedSearchobj.savesearch.ConsiderationAmtUpper= 0;
                savedSearchobj.savesearch.County = propertyFilter.County;
                savedSearchobj.savesearch.DealSubType = propertyFilter.DealSubType;
                savedSearchobj.savesearch.DealType = propertyFilter.DealType;
                savedSearchobj.savesearch.LandSizeLower = propertyFilter.LandSizeTo;
                savedSearchobj.savesearch.LandSizeUpper = propertyFilter.LandSizeFrom ;
                savedSearchobj.savesearch.Location = Location;
                savedSearchobj.savesearch.PropertyName = propertyFilter.PropertyName;
                savedSearchobj.savesearch.Radius = 0;
                savedSearchobj.savesearch.RentAmountLower = propertyFilter.RentAmountFrom;
                savedSearchobj.savesearch.RentAmountUpper = propertyFilter.RentAmountTo;
                savedSearchobj.savesearch.RentedSizeLower = propertyFilter.RentSizeFrom;
                savedSearchobj.savesearch.RentedSizeUpper = propertyFilter.RentSizeTo;
                if (propertyFilter.SalesAnuallyFrom == null && propertyFilter.SalesAnuallyTo == null)
                {
                    if (propertyFilter.SalesMonthlyFrom == null && propertyFilter.SalesMonthlyTo == null)
                    {
                        if (propertyFilter.SalesWeeklyFrom == null && propertyFilter.SalesWeeklyTo == null)
                        {
                            savedSearchobj.savesearch.SalesLower = 0;
                            savedSearchobj.savesearch.SalesUpper = 0;
                        }
                        else
                        {
                            savedSearchobj.savesearch.SalesLower = propertyFilter.SalesWeeklyFrom;
                            savedSearchobj.savesearch.SalesUpper = propertyFilter.SalesWeeklyTo;
                        }
                    }
                    else
                    {
                        savedSearchobj.savesearch.SalesLower = propertyFilter.SalesMonthlyFrom;
                        savedSearchobj.savesearch.SalesUpper = propertyFilter.SalesMonthlyTo;
                    }
                }
                else
                {
                    savedSearchobj.savesearch.SalesLower = propertyFilter.SalesAnuallyFrom;
                    savedSearchobj.savesearch.SalesUpper = propertyFilter.SalesAnuallyTo;
                }
                savedSearchobj.savesearch.SalesPeriodFrom = propertyFilter.SalesPeriodFrom;
                savedSearchobj.savesearch.SalesPeriodTo = propertyFilter.SalesPeriodTo;
                savedSearchobj.savesearch.SearchSavedBy = propertyFilter.LoggedInUser;
                savedSearchobj.savesearch.SizeLower = propertyFilter.SizeTo;
                savedSearchobj.savesearch.SizeUpper = propertyFilter.SizeFrom;
                savedSearchobj.savesearch.State = propertyFilter.State;
                savedSearchobj.savesearch.Zipcode = propertyFilter.Zipcode;
            }
            string str = ViewBag.hdnSavedSearchDelete;
            var client = new HttpClient();
            var UserUrl = Url.RouteUrl(
                "DefaultApi",
                new { httproute = "", controller = "SavedSearch" },
                Request.Url.Scheme
                );
            var user = client.PostAsJsonAsync(UserUrl, savedSearchobj)
                .ContinueWith((userWithResponse) =>
                {
                    var response = userWithResponse.Result;
                    response.EnsureSuccessStatusCode();
                    string Message = response.Content.ReadAsAsync<String>().Result;
                    userWithResponse.Wait();
                });
            user.Wait();

            return Json(1);
        }

         [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OpenSavedSearch(Int32 savedSearchId)
        {
            PropertyFilterDB propfilters = new PropertyFilterDB();
            
            //This needs to be called from API controller
            PropertySavedSearchDB savedSearchobj = new PropertySavedSearchDB();
            usp_GetSavedSearchDetails_Result savedSearchDetail = savedSearchobj.GetSavedSearchDetail(savedSearchId);  
            //Object savedSearchDetail = (usp_GetSavedSearchDetails_Result)savedSearchobj.GetSavedSearchDetail(savedSearchId);

            propfilters.PropertyName = savedSearchDetail.PropertyName;
            propfilters.BuildingSizeFrom = savedSearchDetail.BuildingSizeLower;
            propfilters.BuildingSizeTo = savedSearchDetail.BuildingSizeUpper;
            propfilters.Category = savedSearchDetail.Category;
            propfilters.City = savedSearchDetail.City;
            propfilters.County = savedSearchDetail.County;
            propfilters.DealSubType = savedSearchDetail.DealSubType;
            propfilters.DealType = savedSearchDetail.DealType;
            propfilters.Distance = (Int16)savedSearchDetail.Distance;
            propfilters.LandSizeFrom = savedSearchDetail.LandSizeLower;
            propfilters.LandSizeTo = savedSearchDetail.LandSizeUpper;
            propfilters.location = savedSearchDetail.Location;
            propfilters.SalesAnuallyFrom = savedSearchDetail.SalesAnuallyFrom;
            propfilters.SalesAnuallyFrom = savedSearchDetail.SalesAnuallyFrom;
            propfilters.SalesAnuallyTo = savedSearchDetail.SalesAnuallyTo;
            propfilters.SalesMonthlyFrom = savedSearchDetail.SalesMonthly;
            propfilters.SalesWeeklyFrom = savedSearchDetail.SalesLower;
            propfilters.SalesWeeklyTo = savedSearchDetail.SalesUpper;
            propfilters.SizeFrom = savedSearchDetail.SizeLower;
            propfilters.SizeTo = savedSearchDetail.SizeUpper;
            propfilters.State = savedSearchDetail.State;
            propfilters.Zipcode = savedSearchDetail.Zipcode;
            propfilters.SalesPeriodFrom = savedSearchDetail.SalesPeriodFrom;
            propfilters.SalesPeriodTo = savedSearchDetail.SalesPeriodTo;
            TempData["propertyFilter"] = propfilters;

            TempData["Location"] = "";//set location to blank so that it does not interfere with earlier search

            //return Json("1");
            var objTemp = "1";

            return Json(objTemp, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("ShowMap", "SearchMap");
        }

    }
}
