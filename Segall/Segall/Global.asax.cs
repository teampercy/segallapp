﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace MVCWebAPITest
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class WebApiApplication : System.Web.HttpApplication
    {


        //public static void RegisterRoutes(RouteCollection routes)
        //{
        //    routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

        //    routes.MapRoute(
        //     "Default", // Route name
        //     "{controller}/{action}/{id}", // URL with parameters
        //     new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
        // );
        //}
        protected void Application_Start()
        {
            //Application["APIControllerPath"] = "/Segall";
            //Application["APIControllerPath"] = "";
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //ValueProviderFactories.Factories.Add(new JsonValueProviderFactory());

        }

        void Session_Start(object sender, EventArgs e)
        {
            //If the session object expires, redirect the user to the homepage
            if (HttpContext.Current.Session["LoggedInUser"] == null && HttpContext.Current.User.Identity.IsAuthenticated == true)
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                HttpContext.Current.Response.Redirect("~/Account/Login");
            }
        }

        void Session_End(object sender, EventArgs e)
        {
            Session.Abandon();
        }

    }
}