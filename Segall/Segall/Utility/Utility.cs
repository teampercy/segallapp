﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Winnovative.WnvHtmlConvert;


namespace Segall.Utility
{
    public class Utility
    {
        public static bool sendEmail(string to, string from, string subject, string body, List<string> attachmentList)
        {
            List<string> toList = new List<string>();
            toList.Add(to);
            bool successFlag = sendEmail(toList, from, subject, body, attachmentList);
            return successFlag;
        }
        public static bool sendEmail(List<string> to, string from, string subject, string body, List<string> attachmentList)
        {
            bool successFlag = false;
            try
            {
                MailMessage mail = new MailMessage();
                mail.BodyEncoding = Encoding.Default;
                mail.IsBodyHtml = true;
                if (to.Count > 0)
                {
                    foreach (string emailAddress in to)
                    {
                        mail.To.Add(emailAddress);
                    }
                }
                //mail.From = new MailAddress(ConfigurationManager.AppSettings["SupportEmailAddress"].ToString(), ConfigurationManager.AppSettings["EmailDispalyName"].ToString());
                mail.From = new MailAddress(from, from);
                mail.Subject = subject;
                mail.Body = body;
                

                foreach (string filePath in attachmentList)
                {
                    if (File.Exists(filePath))
                    {
                        System.Net.Mail.Attachment attachment = new Attachment(filePath);
                        mail.Attachments.Add(attachment);
                    }
                }

                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["SMTPHost"].ToString();
                smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"].ToString());
                
                //NetworkCredential basicAuthenticationInfo = new NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"].ToString(), ConfigurationManager.AppSettings["SMTPPassword"].ToString());
                //smtp.EnableSsl = true;
                //smtp.UseDefaultCredentials = false;
                smtp.UseDefaultCredentials = true;
                //smtp.Credentials = basicAuthenticationInfo;

                smtp.Send(mail);
                successFlag = true;
            }
            catch (Exception ex)
            {
            }

            return successFlag;
        }


        public static byte[] ConvertHTMLStringToPDF(string htmlData)
        {
            try
            {
                string htmlString = htmlData;
                PdfConverter pdfConverter = new PdfConverter();
                // set the license key
                pdfConverter.LicenseKey = "OgmRqJy4kkQWgihnkILiGC3ofMK83tlFSiAVpzjJv9Fa6+ZMh+8fNIirK6M+dxWT";
                // set the converter "P38cBx6AWW7b9c81TjEGxnrazP+J7rOjs+9omJ3TUycauK+cLWdrITM5T59hdW5r"
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Best;
                pdfConverter.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
                pdfConverter.PdfDocumentOptions.ShowHeader = true;
                pdfConverter.PdfDocumentOptions.ShowFooter = true;

                // set to generate selectable pdf or a pdf with embedded image
                pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
                // set the embedded fonts option - optional, by default is false
                pdfConverter.PdfDocumentOptions.EmbedFonts = true;
                // enable the live HTTP links option - optional, by default is true
                pdfConverter.PdfDocumentOptions.LiveUrlsEnabled = true;
                // enable the support for right to left languages , by default false
                pdfConverter.RightToLeftEnabled = false;
                pdfConverter.PdfDocumentOptions.LeftMargin = 5;
                pdfConverter.PdfDocumentOptions.RightMargin = 5;
                pdfConverter.PdfDocumentOptions.TopMargin = 0;
                pdfConverter.PdfDocumentOptions.BottomMargin = 0;
                pdfConverter.PdfHeaderOptions.HeaderText = "";
                pdfConverter.PdfFooterOptions.FooterText = "";
                pdfConverter.PdfHeaderOptions.HeaderHeight = 50;
                pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
                pdfConverter.PdfHeaderOptions.HeaderTextYLocation = 1;
                pdfConverter.PdfFooterOptions.FooterHeight = 25;
                pdfConverter.PdfFooterOptions.PageNumberYLocation = 1;
                pdfConverter.PdfFooterOptions.ShowPageNumber = true;
                // set PDF security options - optional
                pdfConverter.PdfSecurityOptions.CanPrint = true;
                pdfConverter.PdfSecurityOptions.CanEditContent = true;
                pdfConverter.PdfSecurityOptions.UserPassword = "";
                // set PDF document description - optional
                pdfConverter.PdfDocumentInfo.AuthorName = "Segall Groups";
                //ControllerContext controllerContext = new ControllerContext();
                //String thisPageUrl = controllerContext.HttpContext.Request.Url.AbsoluteUri;
                //String baseUrl = thisPageUrl.Substring(0, thisPageUrl.Length - "Home/ConvertThisPageToPdf".Length);
                // Performs the conversion and get the pdf document bytes that you can further 
                // save to a file or send as a browser response
                //
                // The baseURL parameter helps the converter to get the CSS files and images
                // referenced by a relative URL in the HTML string. This option has efect only if the HTML string
                // contains a valid HEAD tag. The converter will automatically inserts a <BASE HREF="baseURL"> tag. 
                byte[] pdfBytes = null;
                pdfBytes = pdfConverter.GetPdfBytesFromHtmlString(htmlString);//, baseURL);

                //Please Write the following file checking on the top before calling all store procedures generatepdf() function

                //string PdfPath = ".pdf";
                //FileInfo fFile = new FileInfo(PdfPath);
                //If Not fFile.Exists Then
                //pdfConverter.SavePdfFromHtmlStringToFile(htmlString, Server.MapPath("\") & "uploads\TotalLossFeeCalculation\Summary Reports\" & strclaim & "_" & calculationid & ".pdf")
                //pdfConverter.SavePdfFromHtmlStringToFile(htmlString, Server.MapPath(PdfPath));
                //End If

                //if (!fFile.Exists)
                //  {
                // MessageBox.Show("File Not Found")
                //========================== Generating pdf ======================================
                //System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                //response.Clear();
                //response.AddHeader("Content-Type", "binary/octet-stream");
                //response.AddHeader("Content-Disposition", "attachment; filename=" + strDate + "_" + DateTime.Now.ToString("ddMMyyyy") + ".pdf; size=" + pdfBytes.Length.ToString());
                //response.Flush();
                //response.BinaryWrite(pdfBytes);
                //response.Flush();
                //response.End();
                // ================================== End here ==================================
                return pdfBytes;
            }
            catch (Exception e)
            { throw new HttpException(404, "Couldn't generate pdf " +e.Message ); }
            
        }
        public static byte[] ConvertHTMLStringToPDFwithImages(string htmlData, string thisPageURL)
        {
            string htmlString = htmlData;

            PdfConverter pdfConverter = new PdfConverter();
            // set the license key
            pdfConverter.LicenseKey = "OgmRqJy4kkQWgihnkILiGC3ofMK83tlFSiAVpzjJv9Fa6+ZMh+8fNIirK6M+dxWT";
            // set the converter "P38cBx6AWW7b9c81TjEGxnrazP+J7rOjs+9omJ3TUycauK+cLWdrITM5T59hdW5r"
            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
            //pdfConverter.PdfDocumentOptions.LeftMargin = 10;
            //pdfConverter.PdfDocumentOptions.RightMargin = 10;
            pdfConverter.PdfDocumentOptions.TopMargin = 5;
            pdfConverter.PdfDocumentOptions.BottomMargin = 5;
            pdfConverter.PdfDocumentOptions.ShowHeader = false;
            pdfConverter.PdfDocumentOptions.ShowFooter = false;

            // set to generate selectable pdf or a pdf with embedded image
            pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
            // set the embedded fonts option - optional, by default is false
            pdfConverter.PdfDocumentOptions.EmbedFonts = true;
            // enable the live HTTP links option - optional, by default is true
            pdfConverter.PdfDocumentOptions.LiveUrlsEnabled = true;
            // enable the support for right to left languages , by default false
            pdfConverter.RightToLeftEnabled = false;
            //pdfConverter.PdfDocumentOptions.LeftMargin = 5;
            //pdfConverter.PdfDocumentOptions.RightMargin = 5;
            //pdfConverter.PdfDocumentOptions.TopMargin = 0;
            //pdfConverter.PdfDocumentOptions.BottomMargin = 0;
            //pdfConverter.PdfHeaderOptions.HeaderText = "";
            //pdfConverter.PdfFooterOptions.FooterText = "";
            //pdfConverter.PdfHeaderOptions.HeaderHeight = 50;
            //pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
            //pdfConverter.PdfHeaderOptions.HeaderTextYLocation = 1;
            //pdfConverter.PdfFooterOptions.FooterHeight = 25;
            //pdfConverter.PdfFooterOptions.PageNumberYLocation = 1;
            //pdfConverter.PdfFooterOptions.ShowPageNumber = true;
            // set PDF security options - optional
            pdfConverter.PdfSecurityOptions.CanPrint = true;
            pdfConverter.PdfSecurityOptions.CanEditContent = true;
            pdfConverter.PdfSecurityOptions.UserPassword = "";
            // set PDF document description - optional
            pdfConverter.PdfDocumentInfo.AuthorName = "Segall Groups";
            //pdfConverter.PdfDocumentOptions.BottomMargin = 1;
            //pdfConverter.PdfDocumentOptions.TopMargin = 1;
            //pdfConverter.PdfFooterOptions.FooterHeight = 25;
        
            //ControllerContext controllerContext = new ControllerContext();

            // Performs the conversion and get the pdf document bytes that you can further 
            // save to a file or send as a browser response
            //
            // The baseURL parameter helps the converter to get the CSS files and images
            // referenced by a relative URL in the HTML string. This option has efect only if the HTML string
            // contains a valid HEAD tag. The converter will automatically inserts a <BASE HREF="baseURL"> tag. 
            byte[] pdfBytes = null;

            //string baseUrl = thisPageURL.Substring(0, thisPageURL.LastIndexOf('/')) + "/";

            pdfBytes = pdfConverter.GetPdfBytesFromHtmlString(htmlString, thisPageURL);//, baseURL);

            //Please Write the following file checking on the top before calling all store procedures generatepdf() function

            //string PdfPath = ".pdf";
            //FileInfo fFile = new FileInfo(PdfPath);
            //If Not fFile.Exists Then
            //pdfConverter.SavePdfFromHtmlStringToFile(htmlString, Server.MapPath("\") & "uploads\TotalLossFeeCalculation\Summary Reports\" & strclaim & "_" & calculationid & ".pdf")
            //pdfConverter.SavePdfFromHtmlStringToFile(htmlString, Server.MapPath(PdfPath));
            //End If

            //if (!fFile.Exists)
            //  {
            // MessageBox.Show("File Not Found")
            //========================== Generating pdf ======================================
            //System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            //response.Clear();
            //response.AddHeader("Content-Type", "binary/octet-stream");
            //response.AddHeader("Content-Disposition", "attachment; filename=" + strDate + "_" + DateTime.Now.ToString("ddMMyyyy") + ".pdf; size=" + pdfBytes.Length.ToString());
            //response.Flush();
            //response.BinaryWrite(pdfBytes);
            //response.Flush();
            //response.End();
            // ================================== End here ==================================
            return pdfBytes;
        }

        public static bool StoreBytesAsFile(byte[] data, string pathWithFileName)
        {
            bool valueToReturn = true;
            try
            {
                File.WriteAllBytes(pathWithFileName, data);
            }
            catch (Exception ex)
            {
                valueToReturn = false;
            }
            return valueToReturn;
        }
    }
}